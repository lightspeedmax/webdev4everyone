<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"> 

  <title>PHP: Errors and error handling - Manual </title>

 <link rel="shortcut icon" href="http://php.net/favicon.ico">
 <link rel="search" type="application/opensearchdescription+xml" href="http://php.net/phpnetimprovedsearch.src" title="Add PHP.net search">
 <link rel="alternate" type="application/atom+xml" href="http://php.net/releases/feed.php" title="PHP Release feed">
 <link rel="alternate" type="application/atom+xml" href="http://php.net/feed.atom" title="PHP: Hypertext Preprocessor">

 <link rel="canonical" href="http://php.net/manual/en/pdo.error-handling.php">
 <link rel="shorturl" href="http://php.net/manual/en/pdo.error-handling.php">
 <link rel="alternate" href="http://php.net/manual/en/pdo.error-handling.php" hreflang="x-default">

 <link rel="contents" href="http://php.net/manual/en/index.php">
 <link rel="index" href="http://php.net/manual/en/book.pdo.php">
 <link rel="prev" href="http://php.net/manual/en/pdo.prepared-statements.php">
 <link rel="next" href="http://php.net/manual/en/pdo.lobs.php">

 <link rel="alternate" href="http://php.net/manual/en/pdo.error-handling.php" hreflang="en">
 <link rel="alternate" href="http://php.net/manual/pt_BR/pdo.error-handling.php" hreflang="pt_BR">
 <link rel="alternate" href="http://php.net/manual/zh/pdo.error-handling.php" hreflang="zh">
 <link rel="alternate" href="http://php.net/manual/fr/pdo.error-handling.php" hreflang="fr">
 <link rel="alternate" href="http://php.net/manual/de/pdo.error-handling.php" hreflang="de">
 <link rel="alternate" href="http://php.net/manual/ja/pdo.error-handling.php" hreflang="ja">
 <link rel="alternate" href="http://php.net/manual/ro/pdo.error-handling.php" hreflang="ro">
 <link rel="alternate" href="http://php.net/manual/ru/pdo.error-handling.php" hreflang="ru">
 <link rel="alternate" href="http://php.net/manual/es/pdo.error-handling.php" hreflang="es">
 <link rel="alternate" href="http://php.net/manual/tr/pdo.error-handling.php" hreflang="tr">

<link rel="stylesheet" type="text/css" href="http://php.net/cached.php?t=1421837618&amp;f=/fonts/Fira/fira.css" media="screen">
<link rel="stylesheet" type="text/css" href="http://php.net/cached.php?t=1421837618&amp;f=/fonts/Font-Awesome/css/fontello.css" media="screen">
<link rel="stylesheet" type="text/css" href="http://php.net/cached.php?t=1478800802&amp;f=/styles/theme-base.css" media="screen">
<link rel="stylesheet" type="text/css" href="http://php.net/cached.php?t=1521070803&amp;f=/styles/theme-medium.css" media="screen">

 <!--[if lte IE 7]>
 <link rel="stylesheet" type="text/css" href="http://php.net/styles/workarounds.ie7.css" media="screen">
 <![endif]-->

 <!--[if lte IE 8]>
 <script type="text/javascript">
  window.brokenIE = true;
 </script>
 <![endif]-->

 <!--[if lte IE 9]>
 <link rel="stylesheet" type="text/css" href="http://php.net/styles/workarounds.ie9.css" media="screen">
 <![endif]-->

 <!--[if IE]>
 <script type="text/javascript" src="http://php.net/js/ext/html5.js"></script>
 <![endif]-->

 <base href="http://php.net/manual/en/pdo.error-handling.php">

</head>
<body class="docs ">

<nav id="head-nav" class="navbar navbar-fixed-top">
  <div class="navbar-inner clearfix">
    <a href="/" class="brand"><img src="/images/logos/php-logo.svg" width="48" height="24" alt="php"></a>
    <div id="mainmenu-toggle-overlay"></div>
    <input type="checkbox" id="mainmenu-toggle">
    <ul class="nav">
      <li class=""><a href="/downloads">Downloads</a></li>
      <li class="active"><a href="/docs.php">Documentation</a></li>
      <li class=""><a href="/get-involved" >Get Involved</a></li>
      <li class=""><a href="/support">Help</a></li>
    </ul>
    <form class="navbar-search" id="topsearch" action="/search.php">
      <input type="hidden" name="show" value="quickref">
      <input type="search" name="pattern" class="search-query" placeholder="Search" accesskey="s">
    </form>
  </div>
  <div id="flash-message"></div>
</nav>
<div class="headsup"><a href='/conferences/index.php#id2018-06-13-1'>php[world] 2018 - Call for Speakers</a></div>
<nav id="trick"><div><dl>
<dt><a href='/manual/en/getting-started.php'>Getting Started</a></dt>
	<dd><a href='/manual/en/introduction.php'>Introduction</a></dd>
	<dd><a href='/manual/en/tutorial.php'>A simple tutorial</a></dd>
<dt><a href='/manual/en/langref.php'>Language Reference</a></dt>
	<dd><a href='/manual/en/language.basic-syntax.php'>Basic syntax</a></dd>
	<dd><a href='/manual/en/language.types.php'>Types</a></dd>
	<dd><a href='/manual/en/language.variables.php'>Variables</a></dd>
	<dd><a href='/manual/en/language.constants.php'>Constants</a></dd>
	<dd><a href='/manual/en/language.expressions.php'>Expressions</a></dd>
	<dd><a href='/manual/en/language.operators.php'>Operators</a></dd>
	<dd><a href='/manual/en/language.control-structures.php'>Control Structures</a></dd>
	<dd><a href='/manual/en/language.functions.php'>Functions</a></dd>
	<dd><a href='/manual/en/language.oop5.php'>Classes and Objects</a></dd>
	<dd><a href='/manual/en/language.namespaces.php'>Namespaces</a></dd>
	<dd><a href='/manual/en/language.errors.php'>Errors</a></dd>
	<dd><a href='/manual/en/language.exceptions.php'>Exceptions</a></dd>
	<dd><a href='/manual/en/language.generators.php'>Generators</a></dd>
	<dd><a href='/manual/en/language.references.php'>References Explained</a></dd>
	<dd><a href='/manual/en/reserved.variables.php'>Predefined Variables</a></dd>
	<dd><a href='/manual/en/reserved.exceptions.php'>Predefined Exceptions</a></dd>
	<dd><a href='/manual/en/reserved.interfaces.php'>Predefined Interfaces and Classes</a></dd>
	<dd><a href='/manual/en/context.php'>Context options and parameters</a></dd>
	<dd><a href='/manual/en/wrappers.php'>Supported Protocols and Wrappers</a></dd>
</dl>
<dl>
<dt><a href='/manual/en/security.php'>Security</a></dt>
	<dd><a href='/manual/en/security.intro.php'>Introduction</a></dd>
	<dd><a href='/manual/en/security.general.php'>General considerations</a></dd>
	<dd><a href='/manual/en/security.cgi-bin.php'>Installed as CGI binary</a></dd>
	<dd><a href='/manual/en/security.apache.php'>Installed as an Apache module</a></dd>
	<dd><a href='/manual/en/security.sessions.php'>Session Security</a></dd>
	<dd><a href='/manual/en/security.filesystem.php'>Filesystem Security</a></dd>
	<dd><a href='/manual/en/security.database.php'>Database Security</a></dd>
	<dd><a href='/manual/en/security.errors.php'>Error Reporting</a></dd>
	<dd><a href='/manual/en/security.globals.php'>Using Register Globals</a></dd>
	<dd><a href='/manual/en/security.variables.php'>User Submitted Data</a></dd>
	<dd><a href='/manual/en/security.magicquotes.php'>Magic Quotes</a></dd>
	<dd><a href='/manual/en/security.hiding.php'>Hiding PHP</a></dd>
	<dd><a href='/manual/en/security.current.php'>Keeping Current</a></dd>
<dt><a href='/manual/en/features.php'>Features</a></dt>
	<dd><a href='/manual/en/features.http-auth.php'>HTTP authentication with PHP</a></dd>
	<dd><a href='/manual/en/features.cookies.php'>Cookies</a></dd>
	<dd><a href='/manual/en/features.sessions.php'>Sessions</a></dd>
	<dd><a href='/manual/en/features.xforms.php'>Dealing with XForms</a></dd>
	<dd><a href='/manual/en/features.file-upload.php'>Handling file uploads</a></dd>
	<dd><a href='/manual/en/features.remote-files.php'>Using remote files</a></dd>
	<dd><a href='/manual/en/features.connection-handling.php'>Connection handling</a></dd>
	<dd><a href='/manual/en/features.persistent-connections.php'>Persistent Database Connections</a></dd>
	<dd><a href='/manual/en/features.safe-mode.php'>Safe Mode</a></dd>
	<dd><a href='/manual/en/features.commandline.php'>Command line usage</a></dd>
	<dd><a href='/manual/en/features.gc.php'>Garbage Collection</a></dd>
	<dd><a href='/manual/en/features.dtrace.php'>DTrace Dynamic Tracing</a></dd>
</dl>
<dl>
<dt><a href='/manual/en/funcref.php'>Function Reference</a></dt>
	<dd><a href='/manual/en/refs.basic.php.php'>Affecting PHP's Behaviour</a></dd>
	<dd><a href='/manual/en/refs.utilspec.audio.php'>Audio Formats Manipulation</a></dd>
	<dd><a href='/manual/en/refs.remote.auth.php'>Authentication Services</a></dd>
	<dd><a href='/manual/en/refs.utilspec.cmdline.php'>Command Line Specific Extensions</a></dd>
	<dd><a href='/manual/en/refs.compression.php'>Compression and Archive Extensions</a></dd>
	<dd><a href='/manual/en/refs.creditcard.php'>Credit Card Processing</a></dd>
	<dd><a href='/manual/en/refs.crypto.php'>Cryptography Extensions</a></dd>
	<dd><a href='/manual/en/refs.database.php'>Database Extensions</a></dd>
	<dd><a href='/manual/en/refs.calendar.php'>Date and Time Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.fileprocess.file.php'>File System Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.international.php'>Human Language and Character Encoding Support</a></dd>
	<dd><a href='/manual/en/refs.utilspec.image.php'>Image Processing and Generation</a></dd>
	<dd><a href='/manual/en/refs.remote.mail.php'>Mail Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.math.php'>Mathematical Extensions</a></dd>
	<dd><a href='/manual/en/refs.utilspec.nontext.php'>Non-Text MIME Output</a></dd>
	<dd><a href='/manual/en/refs.fileprocess.process.php'>Process Control Extensions</a></dd>
	<dd><a href='/manual/en/refs.basic.other.php'>Other Basic Extensions</a></dd>
	<dd><a href='/manual/en/refs.remote.other.php'>Other Services</a></dd>
	<dd><a href='/manual/en/refs.search.php'>Search Engine Extensions</a></dd>
	<dd><a href='/manual/en/refs.utilspec.server.php'>Server Specific Extensions</a></dd>
	<dd><a href='/manual/en/refs.basic.session.php'>Session Extensions</a></dd>
	<dd><a href='/manual/en/refs.basic.text.php'>Text Processing</a></dd>
	<dd><a href='/manual/en/refs.basic.vartype.php'>Variable and Type Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.webservice.php'>Web Services</a></dd>
	<dd><a href='/manual/en/refs.utilspec.windows.php'>Windows Only Extensions</a></dd>
	<dd><a href='/manual/en/refs.xml.php'>XML Manipulation</a></dd>
	<dd><a href='/manual/en/refs.ui.php'>GUI Extensions</a></dd>
</dl>
<dl>
<dt>Keyboard Shortcuts</dt><dt>?</dt>
<dd>This help</dd>
<dt>j</dt>
<dd>Next menu item</dd>
<dt>k</dt>
<dd>Previous menu item</dd>
<dt>g p</dt>
<dd>Previous man page</dd>
<dt>g n</dt>
<dd>Next man page</dd>
<dt>G</dt>
<dd>Scroll to bottom</dd>
<dt>g g</dt>
<dd>Scroll to top</dd>
<dt>g h</dt>
<dd>Goto homepage</dd>
<dt>g s</dt>
<dd>Goto search<br>(current page)</dd>
<dt>/</dt>
<dd>Focus search box</dd>
</dl></div></nav>
<div id="goto">
    <div class="search">
         <div class="text"></div>
         <div class="results"><ul></ul></div>
   </div>
</div>

  <div id="breadcrumbs" class="clearfix">
    <div id="breadcrumbs-inner">
          <div class="next">
        <a href="pdo.lobs.php">
          Large Objects (LOBs) &raquo;
        </a>
      </div>
              <div class="prev">
        <a href="pdo.prepared-statements.php">
          &laquo; Prepared statements and stored procedures        </a>
      </div>
          <ul>
            <li><a href='index.php'>PHP Manual</a></li>      <li><a href='funcref.php'>Function Reference</a></li>      <li><a href='refs.database.php'>Database Extensions</a></li>      <li><a href='refs.database.abstract.php'>Abstraction Layers</a></li>      <li><a href='book.pdo.php'>PDO</a></li>      </ul>
    </div>
  </div>




<div id="layout" class="clearfix">
  <section id="layout-content">
  <div class="page-tools">
    <div class="change-language">
      <form action="/manual/change.php" method="get" id="changelang" name="changelang">
        <fieldset>
          <label for="changelang-langs">Change language:</label>
          <select onchange="document.changelang.submit()" name="page" id="changelang-langs">
            <option value='en/pdo.error-handling.php' selected="selected">English</option>
            <option value='pt_BR/pdo.error-handling.php'>Brazilian Portuguese</option>
            <option value='zh/pdo.error-handling.php'>Chinese (Simplified)</option>
            <option value='fr/pdo.error-handling.php'>French</option>
            <option value='de/pdo.error-handling.php'>German</option>
            <option value='ja/pdo.error-handling.php'>Japanese</option>
            <option value='ro/pdo.error-handling.php'>Romanian</option>
            <option value='ru/pdo.error-handling.php'>Russian</option>
            <option value='es/pdo.error-handling.php'>Spanish</option>
            <option value='tr/pdo.error-handling.php'>Turkish</option>
            <option value="help-translate.php">Other</option>
          </select>
        </fieldset>
      </form>
    </div>
    <div class="edit-bug">
      <a href="https://edit.php.net/?project=PHP&amp;perm=en/pdo.error-handling.php">Edit</a>
      <a href="https://bugs.php.net/report.php?bug_type=Documentation+problem&amp;manpage=pdo.error-handling">Report a Bug</a>
    </div>
  </div><div id="pdo.error-handling" class="chapter">
 <h1>Errors and error handling</h1>

 <p class="para">
  PDO offers you a choice of 3 different error handling strategies, to fit
  your style of application development.
 </p>
 <ul class="itemizedlist">
  <li class="listitem">
   <p class="para">
    <strong><code>PDO::ERRMODE_SILENT</code></strong>
   </p>
   <p class="para">
     This is the default mode. PDO will simply set the error code for you
     to inspect using the <span class="function"><a href="pdo.errorcode.php" class="function">PDO::errorCode()</a></span> and
     <span class="function"><a href="pdo.errorinfo.php" class="function">PDO::errorInfo()</a></span> methods on both the
     statement and database objects; if the error resulted from a call on a
     statement object, you would invoke the
     <span class="function"><a href="pdostatement.errorcode.php" class="function">PDOStatement::errorCode()</a></span> or
     <span class="function"><a href="pdostatement.errorinfo.php" class="function">PDOStatement::errorInfo()</a></span>
     method on that object. If the error resulted from a call on the
     database object, you would invoke those methods on the database object
     instead.
    </p>
  </li>
  <li class="listitem">
   <p class="para">
    <strong><code>PDO::ERRMODE_WARNING</code></strong>
   </p>
   <p class="para">
     In addition to setting the error code, PDO will emit a traditional
     E_WARNING message. This setting is useful during debugging/testing, if
     you just want to see what problems occurred without interrupting the
     flow of the application.
    </p>
   </li>
   <li class="listitem">
    <p class="para">
     <strong><code>PDO::ERRMODE_EXCEPTION</code></strong>
    </p>
    <p class="para">
     In addition to setting the error code, PDO will throw a
     <a href="class.pdoexception.php" class="classname">PDOException</a>
     and set its properties to reflect the error code and error
     information. This setting is also useful during debugging, as it will
     effectively &quot;blow up&quot; the script at the point of the error, very
     quickly pointing a finger at potential problem areas in your code
     (remember: transactions are automatically rolled back if the exception
     causes the script to terminate).
    </p>
    <p class="para">
     Exception mode is also useful because you can structure your error
     handling more clearly than with traditional PHP-style warnings, and
     with less code/nesting than by running in silent mode and explicitly
     checking the return value of each database call.
    </p>
    <p class="para">
     See <a href="language.exceptions.php" class="link">Exceptions</a> for more
     information about Exceptions in PHP.
    </p>
   </li>
 </ul>
 <p class="para">
  PDO standardizes on using SQL-92 SQLSTATE error code strings; individual
  PDO drivers are responsible for mapping their native codes to the
  appropriate SQLSTATE codes.   The <span class="function"><a href="pdo.errorcode.php" class="function">PDO::errorCode()</a></span>
  method returns a single SQLSTATE code. If you need more specific
  information about an error, PDO also offers an
  <span class="function"><a href="pdo.errorinfo.php" class="function">PDO::errorInfo()</a></span> method which returns an array
  containing the SQLSTATE code, the driver specific error code and driver
  specific error string.
 </p>
 
 <p class="para">
  <div class="example" id="example-1022">
   <p><strong>Example #1 Create a PDO instance and set the error mode</strong></p>
   <div class="example-contents">
<div class="phpcode"><code><span style="color: #000000">
<span style="color: #0000BB">&lt;?php<br />$dsn&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #DD0000">'mysql:dbname=testdb;host=127.0.0.1'</span><span style="color: #007700">;<br /></span><span style="color: #0000BB">$user&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #DD0000">'dbuser'</span><span style="color: #007700">;<br /></span><span style="color: #0000BB">$password&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #DD0000">'dbpass'</span><span style="color: #007700">;<br /><br />try&nbsp;{<br />&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #0000BB">$dbh&nbsp;</span><span style="color: #007700">=&nbsp;new&nbsp;</span><span style="color: #0000BB">PDO</span><span style="color: #007700">(</span><span style="color: #0000BB">$dsn</span><span style="color: #007700">,&nbsp;</span><span style="color: #0000BB">$user</span><span style="color: #007700">,&nbsp;</span><span style="color: #0000BB">$password</span><span style="color: #007700">);<br />&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #0000BB">$dbh</span><span style="color: #007700">-&gt;</span><span style="color: #0000BB">setAttribute</span><span style="color: #007700">(</span><span style="color: #0000BB">PDO</span><span style="color: #007700">::</span><span style="color: #0000BB">ATTR_ERRMODE</span><span style="color: #007700">,&nbsp;</span><span style="color: #0000BB">PDO</span><span style="color: #007700">::</span><span style="color: #0000BB">ERRMODE_EXCEPTION</span><span style="color: #007700">);<br />}&nbsp;catch&nbsp;(</span><span style="color: #0000BB">PDOException&nbsp;$e</span><span style="color: #007700">)&nbsp;{<br />&nbsp;&nbsp;&nbsp;&nbsp;echo&nbsp;</span><span style="color: #DD0000">'Connection&nbsp;failed:&nbsp;'&nbsp;</span><span style="color: #007700">.&nbsp;</span><span style="color: #0000BB">$e</span><span style="color: #007700">-&gt;</span><span style="color: #0000BB">getMessage</span><span style="color: #007700">();<br />}<br /><br /></span><span style="color: #0000BB">?&gt;</span>
</span>
</code></div>
   </div>

  </div>
 </p>
 <blockquote class="note"><p><strong class="note">Note</strong>: 
  <p class="para">
   <span class="function"><a href="pdo.construct.php" class="function">PDO::__construct()</a></span> will always throw a <a href="class.pdoexception.php" class="classname">PDOException</a> if the connection fails
   regardless of which <strong><code>PDO::ATTR_ERRMODE</code></strong> is currently set. Uncaught Exceptions are fatal.
  </p>
 </p></blockquote>
 <p class="para">
  <div class="example" id="example-1023">
   <p><strong>Example #2 Create a PDO instance and set the error mode from the constructor</strong></p>
   <div class="example-contents">
<div class="phpcode"><code><span style="color: #000000">
<span style="color: #0000BB">&lt;?php<br />$dsn&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #DD0000">'mysql:dbname=test;host=127.0.0.1'</span><span style="color: #007700">;<br /></span><span style="color: #0000BB">$user&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #DD0000">'googleguy'</span><span style="color: #007700">;<br /></span><span style="color: #0000BB">$password&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #DD0000">'googleguy'</span><span style="color: #007700">;<br /><br /></span><span style="color: #FF8000">/*<br />&nbsp;&nbsp;&nbsp;&nbsp;Using&nbsp;try/catch&nbsp;around&nbsp;the&nbsp;constructor&nbsp;is&nbsp;still&nbsp;valid&nbsp;even&nbsp;though&nbsp;we&nbsp;set&nbsp;the&nbsp;ERRMODE&nbsp;to&nbsp;WARNING&nbsp;since<br />&nbsp;&nbsp;&nbsp;&nbsp;PDO::__construct&nbsp;will&nbsp;always&nbsp;throw&nbsp;a&nbsp;PDOException&nbsp;if&nbsp;the&nbsp;connection&nbsp;fails.<br />*/<br /></span><span style="color: #007700">try&nbsp;{<br />&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #0000BB">$dbh&nbsp;</span><span style="color: #007700">=&nbsp;new&nbsp;</span><span style="color: #0000BB">PDO</span><span style="color: #007700">(</span><span style="color: #0000BB">$dsn</span><span style="color: #007700">,&nbsp;</span><span style="color: #0000BB">$user</span><span style="color: #007700">,&nbsp;</span><span style="color: #0000BB">$password</span><span style="color: #007700">,&nbsp;array(</span><span style="color: #0000BB">PDO</span><span style="color: #007700">::</span><span style="color: #0000BB">ATTR_ERRMODE&nbsp;</span><span style="color: #007700">=&gt;&nbsp;</span><span style="color: #0000BB">PDO</span><span style="color: #007700">::</span><span style="color: #0000BB">ERRMODE_WARNING</span><span style="color: #007700">));<br />}&nbsp;catch&nbsp;(</span><span style="color: #0000BB">PDOException&nbsp;$e</span><span style="color: #007700">)&nbsp;{<br />&nbsp;&nbsp;&nbsp;&nbsp;echo&nbsp;</span><span style="color: #DD0000">'Connection&nbsp;failed:&nbsp;'&nbsp;</span><span style="color: #007700">.&nbsp;</span><span style="color: #0000BB">$e</span><span style="color: #007700">-&gt;</span><span style="color: #0000BB">getMessage</span><span style="color: #007700">();<br />&nbsp;&nbsp;&nbsp;&nbsp;exit;<br />}<br /><br /></span><span style="color: #FF8000">//&nbsp;This&nbsp;will&nbsp;cause&nbsp;PDO&nbsp;to&nbsp;throw&nbsp;an&nbsp;error&nbsp;of&nbsp;level&nbsp;E_WARNING&nbsp;instead&nbsp;of&nbsp;an&nbsp;exception&nbsp;(when&nbsp;the&nbsp;table&nbsp;doesn't&nbsp;exist)<br /></span><span style="color: #0000BB">$dbh</span><span style="color: #007700">-&gt;</span><span style="color: #0000BB">query</span><span style="color: #007700">(</span><span style="color: #DD0000">"SELECT&nbsp;wrongcolumn&nbsp;FROM&nbsp;wrongtable"</span><span style="color: #007700">);<br /></span><span style="color: #0000BB">?&gt;</span>
</span>
</code></div>
   </div>

    <div class="example-contents"><p>The above example will output:</p></div>
    <div class="example-contents screen">
<div class="cdata"><pre>
Warning: PDO::query(): SQLSTATE[42S02]: Base table or view not found: 1146 Table &#039;test.wrongtable&#039; doesn&#039;t exist in
/tmp/pdo_test.php on line 18
</pre></div>
    </div>
  </div>
 </p>
</div>

<section id="usernotes">
 <div class="head">
  <span class="action"><a href="/manual/add-note.php?sect=pdo.error-handling&amp;redirect=http://php.net/manual/en/pdo.error-handling.php"><img src='/images/notes-add@2x.png' alt='add a note' width='12' height='12'> <small>add a note</small></a></span>
  <h3 class="title">User Contributed Notes <span class="count">1 note</span></h3>
 </div><div id="allnotes">
  <div class="note" id="117365">  <div class="votes">
    <div id="Vu117365">
    <a href="/manual/vote-note.php?id=117365&amp;page=pdo.error-handling&amp;vote=up" title="Vote up!" class="usernotes-voteu">up</a>
    </div>
    <div id="Vd117365">
    <a href="/manual/vote-note.php?id=117365&amp;page=pdo.error-handling&amp;vote=down" title="Vote down!" class="usernotes-voted">down</a>
    </div>
    <div class="tally" id="V117365" title="46% like this...">
    -5
    </div>
  </div>
  <a href="#117365" class="name">
  <strong class="user"><em>Praveen Raj</em></strong></a><a class="genanchor" href="#117365"> &para;</a><div class="date" title="2015-05-27 07:23"><strong>3 years ago</strong></div>
  <div class="text" id="Hcom117365">
<div class="phpcode"><code><span class="html">
Setting the PDO::ATTR_ERRMODE to PDO::ERRMODE_EXCEPTION applies to both PDO and PDO::PDOStatement objects. Also, exceptions are thrown by: PDO::beginTransaction(), PDO::prepare(), PDOStatement::execute(), PDO::commit(), PDOStatement::fetch(),&nbsp; PDOStatement::fetchAll() and so on... Some of these are specified in their respective documentations as to return 'false' in case of an error.</span>
</code></div>
  </div>
 </div></div>

 <div class="foot"><a href="/manual/add-note.php?sect=pdo.error-handling&amp;redirect=http://php.net/manual/en/pdo.error-handling.php"><img src='/images/notes-add@2x.png' alt='add a note' width='12' height='12'> <small>add a note</small></a></div>
</section>    </section><!-- layout-content -->
        <aside class='layout-menu'>
    
        <ul class='parent-menu-list'>
                                    <li>
                <a href="book.pdo.php">PDO</a>
    
                                    <ul class='child-menu-list'>
    
                          
                        <li class="">
                            <a href="intro.pdo.php" title="Introduction">Introduction</a>
                        </li>
                          
                        <li class="">
                            <a href="pdo.setup.php" title="Installing/Configuring">Installing/Configuring</a>
                        </li>
                          
                        <li class="">
                            <a href="pdo.constants.php" title="Predefined Constants">Predefined Constants</a>
                        </li>
                          
                        <li class="">
                            <a href="pdo.connections.php" title="Connections and Connection management">Connections and Connection management</a>
                        </li>
                          
                        <li class="">
                            <a href="pdo.transactions.php" title="Transactions and auto-&#8203;commit">Transactions and auto-&#8203;commit</a>
                        </li>
                          
                        <li class="">
                            <a href="pdo.prepared-statements.php" title="Prepared statements and stored procedures">Prepared statements and stored procedures</a>
                        </li>
                          
                        <li class="current">
                            <a href="pdo.error-handling.php" title="Errors and error handling">Errors and error handling</a>
                        </li>
                          
                        <li class="">
                            <a href="pdo.lobs.php" title="Large Objects (LOBs)">Large Objects (LOBs)</a>
                        </li>
                          
                        <li class="">
                            <a href="class.pdo.php" title="PDO">PDO</a>
                        </li>
                          
                        <li class="">
                            <a href="class.pdostatement.php" title="PDOStatement">PDOStatement</a>
                        </li>
                          
                        <li class="">
                            <a href="class.pdoexception.php" title="PDOException">PDOException</a>
                        </li>
                          
                        <li class="">
                            <a href="pdo.drivers.php" title="PDO Drivers">PDO Drivers</a>
                        </li>
                            
                    </ul>
                    
            </li>
                        
                    </ul>
    </aside>


  </div><!-- layout -->
         
  <footer>
    <div class="container footer-content">
      <div class="row-fluid">
      <ul class="footmenu">
        <li><a href="/copyright.php">Copyright &copy; 2001-2018 The PHP Group</a></li>
        <li><a href="/my.php">My PHP.net</a></li>
        <li><a href="/contact.php">Contact</a></li>
        <li><a href="/sites.php">Other PHP.net sites</a></li>
        <li><a href="/mirrors.php">Mirror sites</a></li>
        <li><a href="/privacy.php">Privacy policy</a></li>
      </ul>
      </div>
    </div>
  </footer>

    
 <!-- External and third party libraries. -->
 <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/ext/modernizr.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/ext/hogan-2.0.0.min.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/ext/typeahead.min.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/ext/mousetrap.min.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/search.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1516300802&amp;f=/js/common.js"></script>

<a id="toTop" href="javascript:;"><span id="toTopHover"></span><img width="40" height="40" alt="To Top" src="/images/to-top@2x.png"></a>

</body>
</html>

