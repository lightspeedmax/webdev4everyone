<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"> 

  <title>PHP: Choosing an API - Manual </title>

 <link rel="shortcut icon" href="http://php.net/favicon.ico">
 <link rel="search" type="application/opensearchdescription+xml" href="http://php.net/phpnetimprovedsearch.src" title="Add PHP.net search">
 <link rel="alternate" type="application/atom+xml" href="http://php.net/releases/feed.php" title="PHP Release feed">
 <link rel="alternate" type="application/atom+xml" href="http://php.net/feed.atom" title="PHP: Hypertext Preprocessor">

 <link rel="canonical" href="http://php.net/manual/en/mysqlinfo.api.choosing.php">
 <link rel="shorturl" href="http://php.net/manual/en/mysqlinfo.api.choosing.php">
 <link rel="alternate" href="http://php.net/manual/en/mysqlinfo.api.choosing.php" hreflang="x-default">

 <link rel="contents" href="http://php.net/manual/en/index.php">
 <link rel="index" href="http://php.net/manual/en/mysql.php">
 <link rel="prev" href="http://php.net/manual/en/mysqlinfo.terminology.php">
 <link rel="next" href="http://php.net/manual/en/mysqlinfo.library.choosing.php">

 <link rel="alternate" href="http://php.net/manual/en/mysqlinfo.api.choosing.php" hreflang="en">
 <link rel="alternate" href="http://php.net/manual/pt_BR/mysqlinfo.api.choosing.php" hreflang="pt_BR">
 <link rel="alternate" href="http://php.net/manual/zh/mysqlinfo.api.choosing.php" hreflang="zh">
 <link rel="alternate" href="http://php.net/manual/fr/mysqlinfo.api.choosing.php" hreflang="fr">
 <link rel="alternate" href="http://php.net/manual/de/mysqlinfo.api.choosing.php" hreflang="de">
 <link rel="alternate" href="http://php.net/manual/ja/mysqlinfo.api.choosing.php" hreflang="ja">
 <link rel="alternate" href="http://php.net/manual/ro/mysqlinfo.api.choosing.php" hreflang="ro">
 <link rel="alternate" href="http://php.net/manual/ru/mysqlinfo.api.choosing.php" hreflang="ru">
 <link rel="alternate" href="http://php.net/manual/es/mysqlinfo.api.choosing.php" hreflang="es">
 <link rel="alternate" href="http://php.net/manual/tr/mysqlinfo.api.choosing.php" hreflang="tr">

<link rel="stylesheet" type="text/css" href="http://php.net/cached.php?t=1421837618&amp;f=/fonts/Fira/fira.css" media="screen">
<link rel="stylesheet" type="text/css" href="http://php.net/cached.php?t=1421837618&amp;f=/fonts/Font-Awesome/css/fontello.css" media="screen">
<link rel="stylesheet" type="text/css" href="http://php.net/cached.php?t=1478800802&amp;f=/styles/theme-base.css" media="screen">
<link rel="stylesheet" type="text/css" href="http://php.net/cached.php?t=1521070803&amp;f=/styles/theme-medium.css" media="screen">

 <!--[if lte IE 7]>
 <link rel="stylesheet" type="text/css" href="http://php.net/styles/workarounds.ie7.css" media="screen">
 <![endif]-->

 <!--[if lte IE 8]>
 <script type="text/javascript">
  window.brokenIE = true;
 </script>
 <![endif]-->

 <!--[if lte IE 9]>
 <link rel="stylesheet" type="text/css" href="http://php.net/styles/workarounds.ie9.css" media="screen">
 <![endif]-->

 <!--[if IE]>
 <script type="text/javascript" src="http://php.net/js/ext/html5.js"></script>
 <![endif]-->

 <base href="http://php.net/manual/en/mysqlinfo.api.choosing.php">

</head>
<body class="docs ">

<nav id="head-nav" class="navbar navbar-fixed-top">
  <div class="navbar-inner clearfix">
    <a href="/" class="brand"><img src="/images/logos/php-logo.svg" width="48" height="24" alt="php"></a>
    <div id="mainmenu-toggle-overlay"></div>
    <input type="checkbox" id="mainmenu-toggle">
    <ul class="nav">
      <li class=""><a href="/downloads">Downloads</a></li>
      <li class="active"><a href="/docs.php">Documentation</a></li>
      <li class=""><a href="/get-involved" >Get Involved</a></li>
      <li class=""><a href="/support">Help</a></li>
    </ul>
    <form class="navbar-search" id="topsearch" action="/search.php">
      <input type="hidden" name="show" value="quickref">
      <input type="search" name="pattern" class="search-query" placeholder="Search" accesskey="s">
    </form>
  </div>
  <div id="flash-message"></div>
</nav>
<div class="headsup"><a href='/conferences/index.php#id2018-06-13-1'>php[world] 2018 - Call for Speakers</a></div>
<nav id="trick"><div><dl>
<dt><a href='/manual/en/getting-started.php'>Getting Started</a></dt>
	<dd><a href='/manual/en/introduction.php'>Introduction</a></dd>
	<dd><a href='/manual/en/tutorial.php'>A simple tutorial</a></dd>
<dt><a href='/manual/en/langref.php'>Language Reference</a></dt>
	<dd><a href='/manual/en/language.basic-syntax.php'>Basic syntax</a></dd>
	<dd><a href='/manual/en/language.types.php'>Types</a></dd>
	<dd><a href='/manual/en/language.variables.php'>Variables</a></dd>
	<dd><a href='/manual/en/language.constants.php'>Constants</a></dd>
	<dd><a href='/manual/en/language.expressions.php'>Expressions</a></dd>
	<dd><a href='/manual/en/language.operators.php'>Operators</a></dd>
	<dd><a href='/manual/en/language.control-structures.php'>Control Structures</a></dd>
	<dd><a href='/manual/en/language.functions.php'>Functions</a></dd>
	<dd><a href='/manual/en/language.oop5.php'>Classes and Objects</a></dd>
	<dd><a href='/manual/en/language.namespaces.php'>Namespaces</a></dd>
	<dd><a href='/manual/en/language.errors.php'>Errors</a></dd>
	<dd><a href='/manual/en/language.exceptions.php'>Exceptions</a></dd>
	<dd><a href='/manual/en/language.generators.php'>Generators</a></dd>
	<dd><a href='/manual/en/language.references.php'>References Explained</a></dd>
	<dd><a href='/manual/en/reserved.variables.php'>Predefined Variables</a></dd>
	<dd><a href='/manual/en/reserved.exceptions.php'>Predefined Exceptions</a></dd>
	<dd><a href='/manual/en/reserved.interfaces.php'>Predefined Interfaces and Classes</a></dd>
	<dd><a href='/manual/en/context.php'>Context options and parameters</a></dd>
	<dd><a href='/manual/en/wrappers.php'>Supported Protocols and Wrappers</a></dd>
</dl>
<dl>
<dt><a href='/manual/en/security.php'>Security</a></dt>
	<dd><a href='/manual/en/security.intro.php'>Introduction</a></dd>
	<dd><a href='/manual/en/security.general.php'>General considerations</a></dd>
	<dd><a href='/manual/en/security.cgi-bin.php'>Installed as CGI binary</a></dd>
	<dd><a href='/manual/en/security.apache.php'>Installed as an Apache module</a></dd>
	<dd><a href='/manual/en/security.sessions.php'>Session Security</a></dd>
	<dd><a href='/manual/en/security.filesystem.php'>Filesystem Security</a></dd>
	<dd><a href='/manual/en/security.database.php'>Database Security</a></dd>
	<dd><a href='/manual/en/security.errors.php'>Error Reporting</a></dd>
	<dd><a href='/manual/en/security.globals.php'>Using Register Globals</a></dd>
	<dd><a href='/manual/en/security.variables.php'>User Submitted Data</a></dd>
	<dd><a href='/manual/en/security.magicquotes.php'>Magic Quotes</a></dd>
	<dd><a href='/manual/en/security.hiding.php'>Hiding PHP</a></dd>
	<dd><a href='/manual/en/security.current.php'>Keeping Current</a></dd>
<dt><a href='/manual/en/features.php'>Features</a></dt>
	<dd><a href='/manual/en/features.http-auth.php'>HTTP authentication with PHP</a></dd>
	<dd><a href='/manual/en/features.cookies.php'>Cookies</a></dd>
	<dd><a href='/manual/en/features.sessions.php'>Sessions</a></dd>
	<dd><a href='/manual/en/features.xforms.php'>Dealing with XForms</a></dd>
	<dd><a href='/manual/en/features.file-upload.php'>Handling file uploads</a></dd>
	<dd><a href='/manual/en/features.remote-files.php'>Using remote files</a></dd>
	<dd><a href='/manual/en/features.connection-handling.php'>Connection handling</a></dd>
	<dd><a href='/manual/en/features.persistent-connections.php'>Persistent Database Connections</a></dd>
	<dd><a href='/manual/en/features.safe-mode.php'>Safe Mode</a></dd>
	<dd><a href='/manual/en/features.commandline.php'>Command line usage</a></dd>
	<dd><a href='/manual/en/features.gc.php'>Garbage Collection</a></dd>
	<dd><a href='/manual/en/features.dtrace.php'>DTrace Dynamic Tracing</a></dd>
</dl>
<dl>
<dt><a href='/manual/en/funcref.php'>Function Reference</a></dt>
	<dd><a href='/manual/en/refs.basic.php.php'>Affecting PHP's Behaviour</a></dd>
	<dd><a href='/manual/en/refs.utilspec.audio.php'>Audio Formats Manipulation</a></dd>
	<dd><a href='/manual/en/refs.remote.auth.php'>Authentication Services</a></dd>
	<dd><a href='/manual/en/refs.utilspec.cmdline.php'>Command Line Specific Extensions</a></dd>
	<dd><a href='/manual/en/refs.compression.php'>Compression and Archive Extensions</a></dd>
	<dd><a href='/manual/en/refs.creditcard.php'>Credit Card Processing</a></dd>
	<dd><a href='/manual/en/refs.crypto.php'>Cryptography Extensions</a></dd>
	<dd><a href='/manual/en/refs.database.php'>Database Extensions</a></dd>
	<dd><a href='/manual/en/refs.calendar.php'>Date and Time Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.fileprocess.file.php'>File System Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.international.php'>Human Language and Character Encoding Support</a></dd>
	<dd><a href='/manual/en/refs.utilspec.image.php'>Image Processing and Generation</a></dd>
	<dd><a href='/manual/en/refs.remote.mail.php'>Mail Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.math.php'>Mathematical Extensions</a></dd>
	<dd><a href='/manual/en/refs.utilspec.nontext.php'>Non-Text MIME Output</a></dd>
	<dd><a href='/manual/en/refs.fileprocess.process.php'>Process Control Extensions</a></dd>
	<dd><a href='/manual/en/refs.basic.other.php'>Other Basic Extensions</a></dd>
	<dd><a href='/manual/en/refs.remote.other.php'>Other Services</a></dd>
	<dd><a href='/manual/en/refs.search.php'>Search Engine Extensions</a></dd>
	<dd><a href='/manual/en/refs.utilspec.server.php'>Server Specific Extensions</a></dd>
	<dd><a href='/manual/en/refs.basic.session.php'>Session Extensions</a></dd>
	<dd><a href='/manual/en/refs.basic.text.php'>Text Processing</a></dd>
	<dd><a href='/manual/en/refs.basic.vartype.php'>Variable and Type Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.webservice.php'>Web Services</a></dd>
	<dd><a href='/manual/en/refs.utilspec.windows.php'>Windows Only Extensions</a></dd>
	<dd><a href='/manual/en/refs.xml.php'>XML Manipulation</a></dd>
	<dd><a href='/manual/en/refs.ui.php'>GUI Extensions</a></dd>
</dl>
<dl>
<dt>Keyboard Shortcuts</dt><dt>?</dt>
<dd>This help</dd>
<dt>j</dt>
<dd>Next menu item</dd>
<dt>k</dt>
<dd>Previous menu item</dd>
<dt>g p</dt>
<dd>Previous man page</dd>
<dt>g n</dt>
<dd>Next man page</dd>
<dt>G</dt>
<dd>Scroll to bottom</dd>
<dt>g g</dt>
<dd>Scroll to top</dd>
<dt>g h</dt>
<dd>Goto homepage</dd>
<dt>g s</dt>
<dd>Goto search<br>(current page)</dd>
<dt>/</dt>
<dd>Focus search box</dd>
</dl></div></nav>
<div id="goto">
    <div class="search">
         <div class="text"></div>
         <div class="results"><ul></ul></div>
   </div>
</div>

  <div id="breadcrumbs" class="clearfix">
    <div id="breadcrumbs-inner">
          <div class="next">
        <a href="mysqlinfo.library.choosing.php">
          Choosing a library &raquo;
        </a>
      </div>
              <div class="prev">
        <a href="mysqlinfo.terminology.php">
          &laquo; Terminology overview        </a>
      </div>
          <ul>
            <li><a href='index.php'>PHP Manual</a></li>      <li><a href='funcref.php'>Function Reference</a></li>      <li><a href='refs.database.php'>Database Extensions</a></li>      <li><a href='refs.database.vendors.php'>Vendor Specific Database Extensions</a></li>      <li><a href='set.mysqlinfo.php'>MySQL</a></li>      <li><a href='mysql.php'>Overview of the MySQL PHP drivers</a></li>      </ul>
    </div>
  </div>




<div id="layout" class="clearfix">
  <section id="layout-content">
  <div class="page-tools">
    <div class="change-language">
      <form action="/manual/change.php" method="get" id="changelang" name="changelang">
        <fieldset>
          <label for="changelang-langs">Change language:</label>
          <select onchange="document.changelang.submit()" name="page" id="changelang-langs">
            <option value='en/mysqlinfo.api.choosing.php' selected="selected">English</option>
            <option value='pt_BR/mysqlinfo.api.choosing.php'>Brazilian Portuguese</option>
            <option value='zh/mysqlinfo.api.choosing.php'>Chinese (Simplified)</option>
            <option value='fr/mysqlinfo.api.choosing.php'>French</option>
            <option value='de/mysqlinfo.api.choosing.php'>German</option>
            <option value='ja/mysqlinfo.api.choosing.php'>Japanese</option>
            <option value='ro/mysqlinfo.api.choosing.php'>Romanian</option>
            <option value='ru/mysqlinfo.api.choosing.php'>Russian</option>
            <option value='es/mysqlinfo.api.choosing.php'>Spanish</option>
            <option value='tr/mysqlinfo.api.choosing.php'>Turkish</option>
            <option value="help-translate.php">Other</option>
          </select>
        </fieldset>
      </form>
    </div>
    <div class="edit-bug">
      <a href="https://edit.php.net/?project=PHP&amp;perm=en/mysqlinfo.api.choosing.php">Edit</a>
      <a href="https://bugs.php.net/report.php?bug_type=Documentation+problem&amp;manpage=mysqlinfo.api.choosing">Report a Bug</a>
    </div>
  </div><div id="mysqlinfo.api.choosing" class="chapter">
   <h1>Choosing an API</h1>

   <p class="para">
    PHP offers three different APIs to connect to MySQL. Below we show
    the APIs provided by the mysql, mysqli, and PDO extensions. Each code snippet
    creates a connection to a MySQL server running on &quot;example.com&quot; using
    the username &quot;user&quot; and the password &quot;password&quot;. And a query is run to
    greet the user.
   </p>
   <p class="para">
    <div class="example" id="example-1816">
     <p><strong>Example #1 Comparing the three MySQL APIs</strong></p>
     <div class="example-contents">
<div class="phpcode"><code><span style="color: #000000">
<span style="color: #0000BB">&lt;?php<br /></span><span style="color: #FF8000">//&nbsp;mysqli<br /></span><span style="color: #0000BB">$mysqli&nbsp;</span><span style="color: #007700">=&nbsp;new&nbsp;</span><span style="color: #0000BB">mysqli</span><span style="color: #007700">(</span><span style="color: #DD0000">"example.com"</span><span style="color: #007700">,&nbsp;</span><span style="color: #DD0000">"user"</span><span style="color: #007700">,&nbsp;</span><span style="color: #DD0000">"password"</span><span style="color: #007700">,&nbsp;</span><span style="color: #DD0000">"database"</span><span style="color: #007700">);<br /></span><span style="color: #0000BB">$result&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #0000BB">$mysqli</span><span style="color: #007700">-&gt;</span><span style="color: #0000BB">query</span><span style="color: #007700">(</span><span style="color: #DD0000">"SELECT&nbsp;'Hello,&nbsp;dear&nbsp;MySQL&nbsp;user!'&nbsp;AS&nbsp;_message&nbsp;FROM&nbsp;DUAL"</span><span style="color: #007700">);<br /></span><span style="color: #0000BB">$row&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #0000BB">$result</span><span style="color: #007700">-&gt;</span><span style="color: #0000BB">fetch_assoc</span><span style="color: #007700">();<br />echo&nbsp;</span><span style="color: #0000BB">htmlentities</span><span style="color: #007700">(</span><span style="color: #0000BB">$row</span><span style="color: #007700">[</span><span style="color: #DD0000">'_message'</span><span style="color: #007700">]);<br /><br /></span><span style="color: #FF8000">//&nbsp;PDO<br /></span><span style="color: #0000BB">$pdo&nbsp;</span><span style="color: #007700">=&nbsp;new&nbsp;</span><span style="color: #0000BB">PDO</span><span style="color: #007700">(</span><span style="color: #DD0000">'mysql:host=example.com;dbname=database'</span><span style="color: #007700">,&nbsp;</span><span style="color: #DD0000">'user'</span><span style="color: #007700">,&nbsp;</span><span style="color: #DD0000">'password'</span><span style="color: #007700">);<br /></span><span style="color: #0000BB">$statement&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #0000BB">$pdo</span><span style="color: #007700">-&gt;</span><span style="color: #0000BB">query</span><span style="color: #007700">(</span><span style="color: #DD0000">"SELECT&nbsp;'Hello,&nbsp;dear&nbsp;MySQL&nbsp;user!'&nbsp;AS&nbsp;_message&nbsp;FROM&nbsp;DUAL"</span><span style="color: #007700">);<br /></span><span style="color: #0000BB">$row&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #0000BB">$statement</span><span style="color: #007700">-&gt;</span><span style="color: #0000BB">fetch</span><span style="color: #007700">(</span><span style="color: #0000BB">PDO</span><span style="color: #007700">::</span><span style="color: #0000BB">FETCH_ASSOC</span><span style="color: #007700">);<br />echo&nbsp;</span><span style="color: #0000BB">htmlentities</span><span style="color: #007700">(</span><span style="color: #0000BB">$row</span><span style="color: #007700">[</span><span style="color: #DD0000">'_message'</span><span style="color: #007700">]);<br /><br /></span><span style="color: #FF8000">//&nbsp;mysql<br /></span><span style="color: #0000BB">$c&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #0000BB">mysql_connect</span><span style="color: #007700">(</span><span style="color: #DD0000">"example.com"</span><span style="color: #007700">,&nbsp;</span><span style="color: #DD0000">"user"</span><span style="color: #007700">,&nbsp;</span><span style="color: #DD0000">"password"</span><span style="color: #007700">);<br /></span><span style="color: #0000BB">mysql_select_db</span><span style="color: #007700">(</span><span style="color: #DD0000">"database"</span><span style="color: #007700">);<br /></span><span style="color: #0000BB">$result&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #0000BB">mysql_query</span><span style="color: #007700">(</span><span style="color: #DD0000">"SELECT&nbsp;'Hello,&nbsp;dear&nbsp;MySQL&nbsp;user!'&nbsp;AS&nbsp;_message&nbsp;FROM&nbsp;DUAL"</span><span style="color: #007700">);<br /></span><span style="color: #0000BB">$row&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #0000BB">mysql_fetch_assoc</span><span style="color: #007700">(</span><span style="color: #0000BB">$result</span><span style="color: #007700">);<br />echo&nbsp;</span><span style="color: #0000BB">htmlentities</span><span style="color: #007700">(</span><span style="color: #0000BB">$row</span><span style="color: #007700">[</span><span style="color: #DD0000">'_message'</span><span style="color: #007700">]);<br /></span><span style="color: #0000BB">?&gt;</span>
</span>
</code></div>
     </div>

    </div>
   </p>
   <p class="para">
    <em class="emphasis">Recommended API</em>
   </p>
   <p class="para">
    It is recommended to use either the <a href="book.mysqli.php" class="link">mysqli</a>
    or <a href="ref.pdo-mysql.php" class="link">PDO_MySQL</a> extensions.
    It is not recommended to use the old <a href="ref.mysql.php" class="link">mysql</a>
    extension for new development, as it was deprecated in PHP 5.5.0 and was
    removed in PHP 7. A detailed feature comparison matrix is provided below.
    The overall performance of all three extensions is considered to be about
    the same. Although the performance of the extension contributes only a
    fraction of the total run time of a PHP web request.  Often, the impact is
    as low as 0.1%.
   </p>
   <p class="para">
    <em class="emphasis">Feature comparison</em>
   </p>
   <table class="doctable informaltable">
    
     <thead>
      <tr>
       <th class="empty">&nbsp;</th>
       <th>ext/mysqli</th>
       <th>PDO_MySQL</th>
       <th>ext/mysql</th>
      </tr>

     </thead>

     <tbody class="tbody">
      <tr>
       <td>PHP version introduced</td>
       <td>5.0</td>
       <td>5.1</td>
       <td>2.0</td>
      </tr>

      <tr>
       <td>Included with PHP 5.x</td>
       <td>Yes</td>
       <td>Yes</td>
       <td>Yes</td>
      </tr>

      <tr>
       <td>Included with PHP 7.x</td>
       <td>Yes</td>
       <td>Yes</td>
       <td>No</td>
      </tr>

      <tr>
       <td>Development status</td>
       <td>Active</td>
       <td>Active</td>
       <td>Maintenance only in 5.x; removed in 7.x</td>
      </tr>

      <tr>
       <td>Lifecycle</td>
       <td>Active</td>
       <td>Active</td>
       <td>Deprecated in 5.x; removed in 7.x</td>
      </tr>

      <tr>
       <td>Recommended for new projects</td>
       <td>Yes</td>
       <td>Yes</td>
       <td>No</td>
      </tr>

      <tr>
       <td>OOP Interface</td>
       <td>Yes</td>
       <td>Yes</td>
       <td>No</td>
      </tr>

      <tr>
       <td>Procedural Interface</td>
       <td>Yes</td>
       <td>No</td>
       <td>Yes</td>
      </tr>

      <tr>
       <td>API supports non-blocking, asynchronous queries with mysqlnd</td>
       <td>Yes</td>
       <td>No</td>
       <td>No</td>
      </tr>

      <tr>
       <td>Persistent Connections</td>
       <td>Yes</td>
       <td>Yes</td>
       <td>Yes</td>
      </tr>

      <tr>
       <td>API supports Charsets</td>
       <td>Yes</td>
       <td>Yes</td>
       <td>Yes</td>
      </tr>

      <tr>
       <td>API supports server-side Prepared Statements</td>
       <td>Yes</td>
       <td>Yes</td>
       <td>No</td>
      </tr>

      <tr>
       <td>API supports client-side Prepared Statements</td>
       <td>No</td>
       <td>Yes</td>
       <td>No</td>
      </tr>

      <tr>
       <td>API supports Stored Procedures</td>
       <td>Yes</td>
       <td>Yes</td>
       <td>No</td>
      </tr>

      <tr>
       <td>API supports Multiple Statements</td>
       <td>Yes</td>
       <td>Most</td>
       <td>No</td>
      </tr>

      <tr>
       <td>API supports Transactions</td>
       <td>Yes</td>
       <td>Yes</td>
       <td>No</td>
      </tr>

      <tr>
       <td>Transactions can be controlled with SQL</td>
       <td>Yes</td>
       <td>Yes</td>
       <td>Yes</td>
      </tr>

      <tr>
       <td>Supports all MySQL 5.1+ functionality</td>
       <td>Yes</td>
       <td>Most</td>
       <td>No</td>
      </tr>

     </tbody>
    
   </table>

  </div>

<section id="usernotes">
 <div class="head">
  <span class="action"><a href="/manual/add-note.php?sect=mysqlinfo.api.choosing&amp;redirect=http://php.net/manual/en/mysqlinfo.api.choosing.php"><img src='/images/notes-add@2x.png' alt='add a note' width='12' height='12'> <small>add a note</small></a></span>
  <h3 class="title">User Contributed Notes <span class="count">4 notes</span></h3>
 </div><div id="allnotes">
  <div class="note" id="109113">  <div class="votes">
    <div id="Vu109113">
    <a href="/manual/vote-note.php?id=109113&amp;page=mysqlinfo.api.choosing&amp;vote=up" title="Vote up!" class="usernotes-voteu">up</a>
    </div>
    <div id="Vd109113">
    <a href="/manual/vote-note.php?id=109113&amp;page=mysqlinfo.api.choosing&amp;vote=down" title="Vote down!" class="usernotes-voted">down</a>
    </div>
    <div class="tally" id="V109113" title="56% like this...">
    32
    </div>
  </div>
  <a href="#109113" class="name">
  <strong class="user"><em>alvaro at demogracia dot com</em></strong></a><a class="genanchor" href="#109113"> &para;</a><div class="date" title="2012-06-21 09:29"><strong>5 years ago</strong></div>
  <div class="text" id="Hcom109113">
<div class="phpcode"><code><span class="html">
Apart from the feature list, I suggest you try out both MySQLi and PDO and find out what API design you like most. MySQLi is more powerful and probably more complex to learn. PDO is more elegant and has the advantage that you only need to learn one PHP API if you need to work with different DBMS in the future.</span>
</code></div>
  </div>
 </div>
  <div class="note" id="120388">  <div class="votes">
    <div id="Vu120388">
    <a href="/manual/vote-note.php?id=120388&amp;page=mysqlinfo.api.choosing&amp;vote=up" title="Vote up!" class="usernotes-voteu">up</a>
    </div>
    <div id="Vd120388">
    <a href="/manual/vote-note.php?id=120388&amp;page=mysqlinfo.api.choosing&amp;vote=down" title="Vote down!" class="usernotes-voted">down</a>
    </div>
    <div class="tally" id="V120388" title="45% like this...">
    -5
    </div>
  </div>
  <a href="#120388" class="name">
  <strong class="user"><em>Anonymous</em></strong></a><a class="genanchor" href="#120388"> &para;</a><div class="date" title="2016-12-29 11:56"><strong>1 year ago</strong></div>
  <div class="text" id="Hcom120388">
<div class="phpcode"><code><span class="html">
These are quite possibly the most paradoxical lines in this table:<br /><br />API supports non-blocking, asynchronous queries with mysqlnd<br />&nbsp; &nbsp;&nbsp; ext/mysqli: Yes&nbsp; &nbsp;&nbsp; PDO_MySQL: No<br />API supports client-side Prepared Statements<br />&nbsp; &nbsp;&nbsp; ext/mysqli: No&nbsp; &nbsp;&nbsp; PDO_MySQL: Yes<br /><br />Apparently it's either asynchronous I/O or the security of bound parameters.</span>
</code></div>
  </div>
 </div>
  <div class="note" id="111647">  <div class="votes">
    <div id="Vu111647">
    <a href="/manual/vote-note.php?id=111647&amp;page=mysqlinfo.api.choosing&amp;vote=up" title="Vote up!" class="usernotes-voteu">up</a>
    </div>
    <div id="Vd111647">
    <a href="/manual/vote-note.php?id=111647&amp;page=mysqlinfo.api.choosing&amp;vote=down" title="Vote down!" class="usernotes-voted">down</a>
    </div>
    <div class="tally" id="V111647" title="41% like this...">
    -25
    </div>
  </div>
  <a href="#111647" class="name">
  <strong class="user"><em>michaeln at associations plus dot see eh</em></strong></a><a class="genanchor" href="#111647"> &para;</a><div class="date" title="2013-03-12 02:49"><strong>5 years ago</strong></div>
  <div class="text" id="Hcom111647">
<div class="phpcode"><code><span class="html">
Another useful consideration to keep in mind when choosing your library is how extensible it is. Chances are, in any sufficiently advanced development scenario, you're going to be extending your database access class to add a method (or multiple methods) for how to handle database errors and alert the development team of errors and whether to have the code fail immediately or fail gracefully serving the user a user-friendly failure notice. <br /><br />For example, I have a class where I have added extra parameters to the query() function (and a few others), which accept the __FILE__ and __LINE__ constants to facilitate tracking issues. If this were not reasonably possible with PDO-mysql for example (not sure, never used it), it may make one option or the other much less likely to be viable for your usage case.</span>
</code></div>
  </div>
 </div>
  <div class="note" id="118406">  <div class="votes">
    <div id="Vu118406">
    <a href="/manual/vote-note.php?id=118406&amp;page=mysqlinfo.api.choosing&amp;vote=up" title="Vote up!" class="usernotes-voteu">up</a>
    </div>
    <div id="Vd118406">
    <a href="/manual/vote-note.php?id=118406&amp;page=mysqlinfo.api.choosing&amp;vote=down" title="Vote down!" class="usernotes-voted">down</a>
    </div>
    <div class="tally" id="V118406" title="23% like this...">
    -130
    </div>
  </div>
  <a href="#118406" class="name">
  <strong class="user"><em>Anonymous</em></strong></a><a class="genanchor" href="#118406"> &para;</a><div class="date" title="2015-12-01 04:18"><strong>2 years ago</strong></div>
  <div class="text" id="Hcom118406">
<div class="phpcode"><code><span class="html">
My suggestion will be to use a library that hides the internals of the specific extension.&nbsp; For example, now that in php 5.5 mysql is deprecated, if you were using&nbsp; PHP Adodb, all you had to do is<br /><br />go from : $cn = NewADOConnection('mysql') to $cn = NewADOConnection('mysqli');<br /><br />This is a huge benefit. Also if you are changing your database from mysql to sql or oracle, is just changing one parameter.&nbsp; I wish someone told me this when I started.</span>
</code></div>
  </div>
 </div></div>

 <div class="foot"><a href="/manual/add-note.php?sect=mysqlinfo.api.choosing&amp;redirect=http://php.net/manual/en/mysqlinfo.api.choosing.php"><img src='/images/notes-add@2x.png' alt='add a note' width='12' height='12'> <small>add a note</small></a></div>
</section>    </section><!-- layout-content -->
        <aside class='layout-menu'>
    
        <ul class='parent-menu-list'>
                                    <li>
                <a href="mysql.php">Overview of the MySQL PHP drivers</a>
    
                                    <ul class='child-menu-list'>
    
                          
                        <li class="">
                            <a href="mysqlinfo.terminology.php" title="Terminology overview">Terminology overview</a>
                        </li>
                          
                        <li class="current">
                            <a href="mysqlinfo.api.choosing.php" title="Choosing an API">Choosing an API</a>
                        </li>
                          
                        <li class="">
                            <a href="mysqlinfo.library.choosing.php" title="Choosing a library">Choosing a library</a>
                        </li>
                          
                        <li class="">
                            <a href="mysqlinfo.concepts.php" title="Concepts">Concepts</a>
                        </li>
                            
                    </ul>
                    
            </li>
                        
                    </ul>
    </aside>


  </div><!-- layout -->
         
  <footer>
    <div class="container footer-content">
      <div class="row-fluid">
      <ul class="footmenu">
        <li><a href="/copyright.php">Copyright &copy; 2001-2018 The PHP Group</a></li>
        <li><a href="/my.php">My PHP.net</a></li>
        <li><a href="/contact.php">Contact</a></li>
        <li><a href="/sites.php">Other PHP.net sites</a></li>
        <li><a href="/mirrors.php">Mirror sites</a></li>
        <li><a href="/privacy.php">Privacy policy</a></li>
      </ul>
      </div>
    </div>
  </footer>

    
 <!-- External and third party libraries. -->
 <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/ext/modernizr.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/ext/hogan-2.0.0.min.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/ext/typeahead.min.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/ext/mousetrap.min.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/search.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1516300802&amp;f=/js/common.js"></script>

<a id="toTop" href="javascript:;"><span id="toTopHover"></span><img width="40" height="40" alt="To Top" src="/images/to-top@2x.png"></a>

</body>
</html>

