<?php
require_once "pdo.php";

$failure = false;
$failmake = false;
//check to see whether user's name is set
if (! isset($_GET['name']) || strlen($_GET['name']) <1){
  die("Name parameter missing");
}

// return to index page when logout button is pressed
if (isset($_POST['Logout'])) {
  header('Location: index.php');
  return;
}
// to insert values into the autos table
if ( isset($_POST['make']) && isset($_POST['year'])
     && isset($_POST['mileage'])) {
    if (!is_numeric($_POST['year']) || !is_numeric($_POST['mileage'])) {
      $failure = "Mileage and year must be numeric";
    }
    if (strlen($_POST['make']) < 1) {
      $failmake = "Make is required";
    }
    if (!$failure && !$failmake) {
        $sql = "INSERT INTO autos (make, year, mileage)
                  VALUES (:make, :year, :mileage)";
        //echo("<pre>\n".$sql."\n</pre>\n");
        $stmt = $pdo->prepare($sql);
        $stmt->execute(array(
            ':make' => $_POST['make'],
            ':year' => $_POST['year'],
            ':mileage' => $_POST['mileage']));
      }
}

// might also check user3.php file for deletes
?>

<!-- html for the input form-->
<html>
<head>
  <title>Laurel Park Autos</title>
</head>
<body>
<h2>Tracking Autos for lpark@umich.edu</h2>

  <form method="post">
  <p>Make: <input type="text" name="make" size="40"></p>
  <p>Year: <input type="INTEGER" name="year"></p>
  <p>Mileage: <input type="INTEGER" name="mileage"></p>
  <p><input type="submit" value="<?=htmlentities("Add")?>"/>
  <input type="submit" name ="logout" value="<?=htmlentities("Logout")?>"/></p>
  </form>
  <?php
  // Note triple not equals and think how badly double
  // not equals would work here...
  if ( $failure !== false ) {
      // Look closely at the use of single and double quotes
      echo('<p style="color: red;">'.htmlentities($failure)."</p>\n");
  }
  if ( $failmake !== false ) {
      // Look closely at the use of single and double quotes
      echo('<p style="color: red;">'.htmlentities($failmake)."</p>\n");
  }
  ?>
  <form method="post">
  <input type="submit" name ="Logout" value="<?=htmlentities("Logout")?>"/>
  </form>
  <h2><?php $_POST ?></h2>
</body>

<html>
<head></head><body><table border="1">
<?php
$stmt = $pdo->query("SELECT make, year, mileage FROM autos");
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
foreach ($rows as $row) {
    echo "<tr><td>";
    echo(htmlentities($row['make']));
    echo("</td><td>");
    echo($row['year']);
    echo("</td><td>");
    echo($row['mileage']);
    echo("</td></tr>\n");
}
?>
</table>
</body>
</html>
