<?php
require_once "pdo.php";
/*--------------------Model ----------------------*/
/* Variables */
$salt = 'XyZzy12*_';
$stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';  // Pw is php123
$failure = false;  // If we have no POST data

/* Handle the Cancel Button */
if ( isset($_POST['cancel'] ) ) {
    // Redirect the browser to game.php
    header("Location: index.php");
    return;
}

/* Checking for Valid email and Password */
if ( isset($_POST['who']) && isset($_POST['pass'])  ) {
    //echo("<p>Handling POST data...</p>\n");

    if ( strlen($_POST['who']) < 1 || strlen($_POST['pass']) < 1 ) {
        $failure = "Email and password required";
    } else {

      $substr = "@";
      if(strpos($_POST['who'], $substr) !== false) {
        // Now query the Access details and confirm validity
        $check = hash('md5', $salt.$_POST['pass']);
        $sql = "SELECT name,email,password FROM users
            WHERE email = :em AND password = :pw";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(array(
            ':em' => "d@d.co.za",//$_POST['who'],
            ':pw' => $check));
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $name = $row['name'];

        if ( $row == TRUE ) {
            // Redirect the browser to game.php
            $logStatus = error_log("Login success ".$_POST['who'],0);//, 3, "c:\xampp\htdocs\w4e\c3\wk2\autos\my-errors.log");
            //var_dump($logStatus);
            header("Location: autos.php?name=".urlencode($name));
            return;
        } else {
            $failure = "Incorrect password";
            error_log("Login fail ".$_POST['who']." $check", 0);
        }
      } else {
        $failure = "Email must have an at-sign (@)";
      }

    }

//-----------------
/*
    $sql = "SELECT name FROM users
        WHERE email = :em AND password = :pw";

    echo "<p>$sql</p>\n";

    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(
        ':em' => $_POST['email'],
        ':pw' => $_POST['password']));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    var_dump($row);
   if ( $row === FALSE ) {
      echo "<h1>Login incorrect.</h1>\n";
   } else {
      echo "<p>Login success.</p>\n";
   }
   */
}

/*-------------------- View ----------------------*/
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
<title>Craig Mullins - Automobile Database</title>
</head>
<body>
<div class="container">
<h1>Please Log In</h1>
<?php
/* Valid if a user has actually logged in */

// Note triple not equals and think how badly double
// not equals would work here...
if ( $failure !== false ) {
    // Look closely at the use of single and double quotes
    echo('<p style="color: red;">'.htmlentities($failure)."</p>\n");
}
?>

<form method="post">
<p><label for="nam">Email:</label>
<input type="text" size="40" name="who"></p>
<p><label for="id_pass">Password:</label>
<input type="password" size="40" name="pass" id="id_pass"></p>
<p>
<input type="submit" value="Log In"/>
<input type="submit" name="cancel" value="Cancel">

</form>
</div>
</body>
