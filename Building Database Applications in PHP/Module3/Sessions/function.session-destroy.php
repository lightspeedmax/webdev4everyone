<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"> 

  <title>PHP: session_destroy - Manual </title>

 <link rel="shortcut icon" href="http://php.net/favicon.ico">
 <link rel="search" type="application/opensearchdescription+xml" href="http://php.net/phpnetimprovedsearch.src" title="Add PHP.net search">
 <link rel="alternate" type="application/atom+xml" href="http://php.net/releases/feed.php" title="PHP Release feed">
 <link rel="alternate" type="application/atom+xml" href="http://php.net/feed.atom" title="PHP: Hypertext Preprocessor">

 <link rel="canonical" href="http://php.net/manual/en/function.session-destroy.php">
 <link rel="shorturl" href="http://php.net/session-destroy">
 <link rel="alternate" href="http://php.net/session-destroy" hreflang="x-default">

 <link rel="contents" href="http://php.net/manual/en/index.php">
 <link rel="index" href="http://php.net/manual/en/ref.session.php">
 <link rel="prev" href="http://php.net/manual/en/function.session-decode.php">
 <link rel="next" href="http://php.net/manual/en/function.session-encode.php">

 <link rel="alternate" href="http://php.net/manual/en/function.session-destroy.php" hreflang="en">
 <link rel="alternate" href="http://php.net/manual/pt_BR/function.session-destroy.php" hreflang="pt_BR">
 <link rel="alternate" href="http://php.net/manual/zh/function.session-destroy.php" hreflang="zh">
 <link rel="alternate" href="http://php.net/manual/fr/function.session-destroy.php" hreflang="fr">
 <link rel="alternate" href="http://php.net/manual/de/function.session-destroy.php" hreflang="de">
 <link rel="alternate" href="http://php.net/manual/ja/function.session-destroy.php" hreflang="ja">
 <link rel="alternate" href="http://php.net/manual/ro/function.session-destroy.php" hreflang="ro">
 <link rel="alternate" href="http://php.net/manual/ru/function.session-destroy.php" hreflang="ru">
 <link rel="alternate" href="http://php.net/manual/es/function.session-destroy.php" hreflang="es">
 <link rel="alternate" href="http://php.net/manual/tr/function.session-destroy.php" hreflang="tr">

<link rel="stylesheet" type="text/css" href="http://php.net/cached.php?t=1421837618&amp;f=/fonts/Fira/fira.css" media="screen">
<link rel="stylesheet" type="text/css" href="http://php.net/cached.php?t=1421837618&amp;f=/fonts/Font-Awesome/css/fontello.css" media="screen">
<link rel="stylesheet" type="text/css" href="http://php.net/cached.php?t=1478800802&amp;f=/styles/theme-base.css" media="screen">
<link rel="stylesheet" type="text/css" href="http://php.net/cached.php?t=1521070803&amp;f=/styles/theme-medium.css" media="screen">

 <!--[if lte IE 7]>
 <link rel="stylesheet" type="text/css" href="http://php.net/styles/workarounds.ie7.css" media="screen">
 <![endif]-->

 <!--[if lte IE 8]>
 <script type="text/javascript">
  window.brokenIE = true;
 </script>
 <![endif]-->

 <!--[if lte IE 9]>
 <link rel="stylesheet" type="text/css" href="http://php.net/styles/workarounds.ie9.css" media="screen">
 <![endif]-->

 <!--[if IE]>
 <script type="text/javascript" src="http://php.net/js/ext/html5.js"></script>
 <![endif]-->

 <base href="http://php.net/manual/en/function.session-destroy.php">

</head>
<body class="docs ">

<nav id="head-nav" class="navbar navbar-fixed-top">
  <div class="navbar-inner clearfix">
    <a href="/" class="brand"><img src="/images/logos/php-logo.svg" width="48" height="24" alt="php"></a>
    <div id="mainmenu-toggle-overlay"></div>
    <input type="checkbox" id="mainmenu-toggle">
    <ul class="nav">
      <li class=""><a href="/downloads">Downloads</a></li>
      <li class="active"><a href="/docs.php">Documentation</a></li>
      <li class=""><a href="/get-involved" >Get Involved</a></li>
      <li class=""><a href="/support">Help</a></li>
    </ul>
    <form class="navbar-search" id="topsearch" action="/search.php">
      <input type="hidden" name="show" value="quickref">
      <input type="search" name="pattern" class="search-query" placeholder="Search" accesskey="s">
    </form>
  </div>
  <div id="flash-message"></div>
</nav>
<div class="headsup"><a href='/conferences/index.php#id2018-06-13-1'>php[world] 2018 - Call for Speakers</a></div>
<nav id="trick"><div><dl>
<dt><a href='/manual/en/getting-started.php'>Getting Started</a></dt>
	<dd><a href='/manual/en/introduction.php'>Introduction</a></dd>
	<dd><a href='/manual/en/tutorial.php'>A simple tutorial</a></dd>
<dt><a href='/manual/en/langref.php'>Language Reference</a></dt>
	<dd><a href='/manual/en/language.basic-syntax.php'>Basic syntax</a></dd>
	<dd><a href='/manual/en/language.types.php'>Types</a></dd>
	<dd><a href='/manual/en/language.variables.php'>Variables</a></dd>
	<dd><a href='/manual/en/language.constants.php'>Constants</a></dd>
	<dd><a href='/manual/en/language.expressions.php'>Expressions</a></dd>
	<dd><a href='/manual/en/language.operators.php'>Operators</a></dd>
	<dd><a href='/manual/en/language.control-structures.php'>Control Structures</a></dd>
	<dd><a href='/manual/en/language.functions.php'>Functions</a></dd>
	<dd><a href='/manual/en/language.oop5.php'>Classes and Objects</a></dd>
	<dd><a href='/manual/en/language.namespaces.php'>Namespaces</a></dd>
	<dd><a href='/manual/en/language.errors.php'>Errors</a></dd>
	<dd><a href='/manual/en/language.exceptions.php'>Exceptions</a></dd>
	<dd><a href='/manual/en/language.generators.php'>Generators</a></dd>
	<dd><a href='/manual/en/language.references.php'>References Explained</a></dd>
	<dd><a href='/manual/en/reserved.variables.php'>Predefined Variables</a></dd>
	<dd><a href='/manual/en/reserved.exceptions.php'>Predefined Exceptions</a></dd>
	<dd><a href='/manual/en/reserved.interfaces.php'>Predefined Interfaces and Classes</a></dd>
	<dd><a href='/manual/en/context.php'>Context options and parameters</a></dd>
	<dd><a href='/manual/en/wrappers.php'>Supported Protocols and Wrappers</a></dd>
</dl>
<dl>
<dt><a href='/manual/en/security.php'>Security</a></dt>
	<dd><a href='/manual/en/security.intro.php'>Introduction</a></dd>
	<dd><a href='/manual/en/security.general.php'>General considerations</a></dd>
	<dd><a href='/manual/en/security.cgi-bin.php'>Installed as CGI binary</a></dd>
	<dd><a href='/manual/en/security.apache.php'>Installed as an Apache module</a></dd>
	<dd><a href='/manual/en/security.sessions.php'>Session Security</a></dd>
	<dd><a href='/manual/en/security.filesystem.php'>Filesystem Security</a></dd>
	<dd><a href='/manual/en/security.database.php'>Database Security</a></dd>
	<dd><a href='/manual/en/security.errors.php'>Error Reporting</a></dd>
	<dd><a href='/manual/en/security.globals.php'>Using Register Globals</a></dd>
	<dd><a href='/manual/en/security.variables.php'>User Submitted Data</a></dd>
	<dd><a href='/manual/en/security.magicquotes.php'>Magic Quotes</a></dd>
	<dd><a href='/manual/en/security.hiding.php'>Hiding PHP</a></dd>
	<dd><a href='/manual/en/security.current.php'>Keeping Current</a></dd>
<dt><a href='/manual/en/features.php'>Features</a></dt>
	<dd><a href='/manual/en/features.http-auth.php'>HTTP authentication with PHP</a></dd>
	<dd><a href='/manual/en/features.cookies.php'>Cookies</a></dd>
	<dd><a href='/manual/en/features.sessions.php'>Sessions</a></dd>
	<dd><a href='/manual/en/features.xforms.php'>Dealing with XForms</a></dd>
	<dd><a href='/manual/en/features.file-upload.php'>Handling file uploads</a></dd>
	<dd><a href='/manual/en/features.remote-files.php'>Using remote files</a></dd>
	<dd><a href='/manual/en/features.connection-handling.php'>Connection handling</a></dd>
	<dd><a href='/manual/en/features.persistent-connections.php'>Persistent Database Connections</a></dd>
	<dd><a href='/manual/en/features.safe-mode.php'>Safe Mode</a></dd>
	<dd><a href='/manual/en/features.commandline.php'>Command line usage</a></dd>
	<dd><a href='/manual/en/features.gc.php'>Garbage Collection</a></dd>
	<dd><a href='/manual/en/features.dtrace.php'>DTrace Dynamic Tracing</a></dd>
</dl>
<dl>
<dt><a href='/manual/en/funcref.php'>Function Reference</a></dt>
	<dd><a href='/manual/en/refs.basic.php.php'>Affecting PHP's Behaviour</a></dd>
	<dd><a href='/manual/en/refs.utilspec.audio.php'>Audio Formats Manipulation</a></dd>
	<dd><a href='/manual/en/refs.remote.auth.php'>Authentication Services</a></dd>
	<dd><a href='/manual/en/refs.utilspec.cmdline.php'>Command Line Specific Extensions</a></dd>
	<dd><a href='/manual/en/refs.compression.php'>Compression and Archive Extensions</a></dd>
	<dd><a href='/manual/en/refs.creditcard.php'>Credit Card Processing</a></dd>
	<dd><a href='/manual/en/refs.crypto.php'>Cryptography Extensions</a></dd>
	<dd><a href='/manual/en/refs.database.php'>Database Extensions</a></dd>
	<dd><a href='/manual/en/refs.calendar.php'>Date and Time Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.fileprocess.file.php'>File System Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.international.php'>Human Language and Character Encoding Support</a></dd>
	<dd><a href='/manual/en/refs.utilspec.image.php'>Image Processing and Generation</a></dd>
	<dd><a href='/manual/en/refs.remote.mail.php'>Mail Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.math.php'>Mathematical Extensions</a></dd>
	<dd><a href='/manual/en/refs.utilspec.nontext.php'>Non-Text MIME Output</a></dd>
	<dd><a href='/manual/en/refs.fileprocess.process.php'>Process Control Extensions</a></dd>
	<dd><a href='/manual/en/refs.basic.other.php'>Other Basic Extensions</a></dd>
	<dd><a href='/manual/en/refs.remote.other.php'>Other Services</a></dd>
	<dd><a href='/manual/en/refs.search.php'>Search Engine Extensions</a></dd>
	<dd><a href='/manual/en/refs.utilspec.server.php'>Server Specific Extensions</a></dd>
	<dd><a href='/manual/en/refs.basic.session.php'>Session Extensions</a></dd>
	<dd><a href='/manual/en/refs.basic.text.php'>Text Processing</a></dd>
	<dd><a href='/manual/en/refs.basic.vartype.php'>Variable and Type Related Extensions</a></dd>
	<dd><a href='/manual/en/refs.webservice.php'>Web Services</a></dd>
	<dd><a href='/manual/en/refs.utilspec.windows.php'>Windows Only Extensions</a></dd>
	<dd><a href='/manual/en/refs.xml.php'>XML Manipulation</a></dd>
	<dd><a href='/manual/en/refs.ui.php'>GUI Extensions</a></dd>
</dl>
<dl>
<dt>Keyboard Shortcuts</dt><dt>?</dt>
<dd>This help</dd>
<dt>j</dt>
<dd>Next menu item</dd>
<dt>k</dt>
<dd>Previous menu item</dd>
<dt>g p</dt>
<dd>Previous man page</dd>
<dt>g n</dt>
<dd>Next man page</dd>
<dt>G</dt>
<dd>Scroll to bottom</dd>
<dt>g g</dt>
<dd>Scroll to top</dd>
<dt>g h</dt>
<dd>Goto homepage</dd>
<dt>g s</dt>
<dd>Goto search<br>(current page)</dd>
<dt>/</dt>
<dd>Focus search box</dd>
</dl></div></nav>
<div id="goto">
    <div class="search">
         <div class="text"></div>
         <div class="results"><ul></ul></div>
   </div>
</div>

  <div id="breadcrumbs" class="clearfix">
    <div id="breadcrumbs-inner">
          <div class="next">
        <a href="function.session-encode.php">
          session_encode &raquo;
        </a>
      </div>
              <div class="prev">
        <a href="function.session-decode.php">
          &laquo; session_decode        </a>
      </div>
          <ul>
            <li><a href='index.php'>PHP Manual</a></li>      <li><a href='funcref.php'>Function Reference</a></li>      <li><a href='refs.basic.session.php'>Session Extensions</a></li>      <li><a href='book.session.php'>Sessions</a></li>      <li><a href='ref.session.php'>Session Functions</a></li>      </ul>
    </div>
  </div>




<div id="layout" class="clearfix">
  <section id="layout-content">
  <div class="page-tools">
    <div class="change-language">
      <form action="/manual/change.php" method="get" id="changelang" name="changelang">
        <fieldset>
          <label for="changelang-langs">Change language:</label>
          <select onchange="document.changelang.submit()" name="page" id="changelang-langs">
            <option value='en/function.session-destroy.php' selected="selected">English</option>
            <option value='pt_BR/function.session-destroy.php'>Brazilian Portuguese</option>
            <option value='zh/function.session-destroy.php'>Chinese (Simplified)</option>
            <option value='fr/function.session-destroy.php'>French</option>
            <option value='de/function.session-destroy.php'>German</option>
            <option value='ja/function.session-destroy.php'>Japanese</option>
            <option value='ro/function.session-destroy.php'>Romanian</option>
            <option value='ru/function.session-destroy.php'>Russian</option>
            <option value='es/function.session-destroy.php'>Spanish</option>
            <option value='tr/function.session-destroy.php'>Turkish</option>
            <option value="help-translate.php">Other</option>
          </select>
        </fieldset>
      </form>
    </div>
    <div class="edit-bug">
      <a href="https://edit.php.net/?project=PHP&amp;perm=en/function.session-destroy.php">Edit</a>
      <a href="https://bugs.php.net/report.php?bug_type=Documentation+problem&amp;manpage=function.session-destroy">Report a Bug</a>
    </div>
  </div><div id="function.session-destroy" class="refentry">
 <div class="refnamediv">
  <h1 class="refname">session_destroy</h1>
  <p class="verinfo">(PHP 4, PHP 5, PHP 7)</p><p class="refpurpose"><span class="refname">session_destroy</span> &mdash; <span class="dc-title">Destroys all data registered to a session</span></p>

 </div>

 <div class="refsect1 description" id="refsect1-function.session-destroy-description">
  <h3 class="title">Description</h3>
  <div class="methodsynopsis dc-description">
   <span class="type">bool</span> <span class="methodname"><strong>session_destroy</strong></span>
    ( <span class="methodparam">void</span>
   )</div>

  <p class="simpara">
   <span class="function"><strong>session_destroy()</strong></span> destroys all of the data associated
   with the current session. It does not unset any of the global variables
   associated with the session, or unset the session cookie.
   To use the session variables again, <span class="function"><a href="function.session-start.php" class="function">session_start()</a></span> has
   to be called.
  </p>
  <blockquote class="note"><p><strong class="note">Note</strong>: 
   <span class="simpara">
     You do not have to call <span class="function"><strong>session_destroy()</strong></span> from usual
     code. Cleanup $_SESSION array rather than destroying session data.
   </span>
  </p></blockquote>
  <p class="para">
   In order to kill the session altogether, the
   session ID must also be unset. If a cookie is used to propagate the
   session ID (default behavior), then the session cookie must be deleted.
   <span class="function"><a href="function.setcookie.php" class="function">setcookie()</a></span> may be used for that.
  </p>
  <p class="para">
   When <a href="session.configuration.php#ini.session.use-strict-mode" class="link">session.use_strict_mode</a>
   is enabled. You do not have to remove obsolete session ID cookie because
   session module will not accept session ID cookie when there is no
   data associated to the session ID and set new session ID cookie.
   Enabling <a href="session.configuration.php#ini.session.use-strict-mode" class="link">session.use_strict_mode</a>
   is recommended for all sites.
  </p>
  <div class="warning"><strong class="warning">Warning</strong>
   <p class="para">
    Immediate session deletion may cause unwanted results. When there is
    concurrent requests, other connections may see sudden session data
    loss. e.g. Requests from JavaScript and/or requests from URL links.
   </p>
   <p class="para">
    Although current session module does not accept empty session ID
    cookie, but immediate session deletion may result in empty session ID
    cookie due to client(browser) side race condition. This will result
    that the client creates many session ID needlessly.
   </p>
   <p class="para">
    To avoid these, you must set deletion time-stamp to $_SESSION and
    reject access while later. Or make sure your application does not
    have concurrent requests. This applies to <span class="function"><a href="function.session-regenerate-id.php" class="function">session_regenerate_id()</a></span> also.
   </p>
  </div>
 </div>


 <div class="refsect1 returnvalues" id="refsect1-function.session-destroy-returnvalues">
  <h3 class="title">Return Values</h3>
  <p class="para">
   Returns <strong><code>TRUE</code></strong> on success or <strong><code>FALSE</code></strong> on failure.
  </p>
 </div>


 <div class="refsect1 examples" id="refsect1-function.session-destroy-examples">
  <h3 class="title">Examples</h3>
  <p class="para">
   <div class="example" id="example-5896">
    <p><strong>Example #1 Destroying a session with <var class="varname"><var class="varname"><a href="reserved.variables.session.php" class="classname">$_SESSION</a></var></var></strong></p>
    <div class="example-contents">
<div class="phpcode"><code><span style="color: #000000">
<span style="color: #0000BB">&lt;?php<br /></span><span style="color: #FF8000">//&nbsp;Initialize&nbsp;the&nbsp;session.<br />//&nbsp;If&nbsp;you&nbsp;are&nbsp;using&nbsp;session_name("something"),&nbsp;don't&nbsp;forget&nbsp;it&nbsp;now!<br /></span><span style="color: #0000BB">session_start</span><span style="color: #007700">();<br /><br /></span><span style="color: #FF8000">//&nbsp;Unset&nbsp;all&nbsp;of&nbsp;the&nbsp;session&nbsp;variables.<br /></span><span style="color: #0000BB">$_SESSION&nbsp;</span><span style="color: #007700">=&nbsp;array();<br /><br /></span><span style="color: #FF8000">//&nbsp;If&nbsp;it's&nbsp;desired&nbsp;to&nbsp;kill&nbsp;the&nbsp;session,&nbsp;also&nbsp;delete&nbsp;the&nbsp;session&nbsp;cookie.<br />//&nbsp;Note:&nbsp;This&nbsp;will&nbsp;destroy&nbsp;the&nbsp;session,&nbsp;and&nbsp;not&nbsp;just&nbsp;the&nbsp;session&nbsp;data!<br /></span><span style="color: #007700">if&nbsp;(</span><span style="color: #0000BB">ini_get</span><span style="color: #007700">(</span><span style="color: #DD0000">"session.use_cookies"</span><span style="color: #007700">))&nbsp;{<br />&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #0000BB">$params&nbsp;</span><span style="color: #007700">=&nbsp;</span><span style="color: #0000BB">session_get_cookie_params</span><span style="color: #007700">();<br />&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #0000BB">setcookie</span><span style="color: #007700">(</span><span style="color: #0000BB">session_name</span><span style="color: #007700">(),&nbsp;</span><span style="color: #DD0000">''</span><span style="color: #007700">,&nbsp;</span><span style="color: #0000BB">time</span><span style="color: #007700">()&nbsp;-&nbsp;</span><span style="color: #0000BB">42000</span><span style="color: #007700">,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #0000BB">$params</span><span style="color: #007700">[</span><span style="color: #DD0000">"path"</span><span style="color: #007700">],&nbsp;</span><span style="color: #0000BB">$params</span><span style="color: #007700">[</span><span style="color: #DD0000">"domain"</span><span style="color: #007700">],<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #0000BB">$params</span><span style="color: #007700">[</span><span style="color: #DD0000">"secure"</span><span style="color: #007700">],&nbsp;</span><span style="color: #0000BB">$params</span><span style="color: #007700">[</span><span style="color: #DD0000">"httponly"</span><span style="color: #007700">]<br />&nbsp;&nbsp;&nbsp;&nbsp;);<br />}<br /><br /></span><span style="color: #FF8000">//&nbsp;Finally,&nbsp;destroy&nbsp;the&nbsp;session.<br /></span><span style="color: #0000BB">session_destroy</span><span style="color: #007700">();<br /></span><span style="color: #0000BB">?&gt;</span>
</span>
</code></div>
    </div>

   </div>
  </p>
 </div>


 <div class="refsect1 notes" id="refsect1-function.session-destroy-notes">
  <h3 class="title">Notes</h3>
  <blockquote class="note"><p><strong class="note">Note</strong>: 
   <p class="para">
    Only use <span class="function"><a href="function.session-unset.php" class="function">session_unset()</a></span> for older deprecated code
    that does not use <var class="varname"><var class="varname"><a href="reserved.variables.session.php" class="classname">$_SESSION</a></var></var>.
   </p>
  </p></blockquote>
 </div>


 <div class="refsect1 seealso" id="refsect1-function.session-destroy-seealso">
  <h3 class="title">See Also</h3>
  <p class="para">
   <ul class="simplelist">
    <li class="member"><a href="session.configuration.php#ini.session.use-strict-mode" class="link">session.use_strict_mode</a></li>
    <li class="member"><span class="function"><a href="function.session-reset.php" class="function" rel="rdfs-seeAlso">session_reset()</a> - Re-initialize session array with original values</span></li>
    <li class="member"><span class="function"><a href="function.session-regenerate-id.php" class="function" rel="rdfs-seeAlso">session_regenerate_id()</a> - Update the current session id with a newly generated one</span></li>
    <li class="member"><span class="function"><a href="function.unset.php" class="function" rel="rdfs-seeAlso">unset()</a> - Unset a given variable</span></li>
    <li class="member"><span class="function"><a href="function.setcookie.php" class="function" rel="rdfs-seeAlso">setcookie()</a> - Send a cookie</span></li>
   </ul>
  </p>
 </div>


</div>
<section id="usernotes">
 <div class="head">
  <span class="action"><a href="/manual/add-note.php?sect=function.session-destroy&amp;redirect=http://php.net/manual/en/function.session-destroy.php"><img src='/images/notes-add@2x.png' alt='add a note' width='12' height='12'> <small>add a note</small></a></span>
  <h3 class="title">User Contributed Notes <span class="count">6 notes</span></h3>
 </div><div id="allnotes">
  <div class="note" id="110043">  <div class="votes">
    <div id="Vu110043">
    <a href="/manual/vote-note.php?id=110043&amp;page=function.session-destroy&amp;vote=up" title="Vote up!" class="usernotes-voteu">up</a>
    </div>
    <div id="Vd110043">
    <a href="/manual/vote-note.php?id=110043&amp;page=function.session-destroy&amp;vote=down" title="Vote down!" class="usernotes-voted">down</a>
    </div>
    <div class="tally" id="V110043" title="62% like this...">
    44
    </div>
  </div>
  <a href="#110043" class="name">
  <strong class="user"><em>Praveen V</em></strong></a><a class="genanchor" href="#110043"> &para;</a><div class="date" title="2012-09-14 04:39"><strong>5 years ago</strong></div>
  <div class="text" id="Hcom110043">
<div class="phpcode"><code><span class="html">
If you want to change the session id on each log in, make sure to use session_regenerate_id(true) during the log in process.<br /><br /><span class="default">&lt;?php<br />session_start</span><span class="keyword">();<br /></span><span class="default">session_regenerate_id</span><span class="keyword">(</span><span class="default">true</span><span class="keyword">);<br /></span><span class="default">?&gt;<br /></span><br />[Edited by moderator (googleguy at php dot net)]</span>
</code></div>
  </div>
 </div>
  <div class="note" id="114709">  <div class="votes">
    <div id="Vu114709">
    <a href="/manual/vote-note.php?id=114709&amp;page=function.session-destroy&amp;vote=up" title="Vote up!" class="usernotes-voteu">up</a>
    </div>
    <div id="Vd114709">
    <a href="/manual/vote-note.php?id=114709&amp;page=function.session-destroy&amp;vote=down" title="Vote down!" class="usernotes-voted">down</a>
    </div>
    <div class="tally" id="V114709" title="61% like this...">
    25
    </div>
  </div>
  <a href="#114709" class="name">
  <strong class="user"><em>Jack Luo</em></strong></a><a class="genanchor" href="#114709"> &para;</a><div class="date" title="2014-03-27 10:08"><strong>4 years ago</strong></div>
  <div class="text" id="Hcom114709">
<div class="phpcode"><code><span class="html">
It took me a while to figure out how to destroy a particular session in php. Note I'm not sure if solution provided below is perfect but it seems work for me. Please feel free to post any easier way to destroy a particular session. Because it's quite useful for functionality of force an user offline.<br /><br />1. If you're using db or memcached to manage session, you can always delete that session entry directly from db or memcached.<br /><br />2. Using generic php session methods to delete a particular session(by session id).<br /><br /><span class="default">&lt;?php<br />$session_id_to_destroy </span><span class="keyword">= </span><span class="string">'nill2if998vhplq9f3pj08vjb1'</span><span class="keyword">;<br /></span><span class="comment">// 1. commit session if it's started.<br /></span><span class="keyword">if (</span><span class="default">session_id</span><span class="keyword">()) {<br />&nbsp; &nbsp; </span><span class="default">session_commit</span><span class="keyword">();<br />}<br /><br /></span><span class="comment">// 2. store current session id<br /></span><span class="default">session_start</span><span class="keyword">();<br /></span><span class="default">$current_session_id </span><span class="keyword">= </span><span class="default">session_id</span><span class="keyword">();<br /></span><span class="default">session_commit</span><span class="keyword">();<br /><br /></span><span class="comment">// 3. hijack then destroy session specified.<br /></span><span class="default">session_id</span><span class="keyword">(</span><span class="default">$session_id_to_destroy</span><span class="keyword">);<br /></span><span class="default">session_start</span><span class="keyword">();<br /></span><span class="default">session_destroy</span><span class="keyword">();<br /></span><span class="default">session_commit</span><span class="keyword">();<br /><br /></span><span class="comment">// 4. restore current session id. If don't restore it, your current session will refer to the session you just destroyed!<br /></span><span class="default">session_id</span><span class="keyword">(</span><span class="default">$current_session_id</span><span class="keyword">);<br /></span><span class="default">session_start</span><span class="keyword">();<br /></span><span class="default">session_commit</span><span class="keyword">();<br /><br /></span><span class="default">?&gt;</span>
</span>
</code></div>
  </div>
 </div>
  <div class="note" id="121315">  <div class="votes">
    <div id="Vu121315">
    <a href="/manual/vote-note.php?id=121315&amp;page=function.session-destroy&amp;vote=up" title="Vote up!" class="usernotes-voteu">up</a>
    </div>
    <div id="Vd121315">
    <a href="/manual/vote-note.php?id=121315&amp;page=function.session-destroy&amp;vote=down" title="Vote down!" class="usernotes-voted">down</a>
    </div>
    <div class="tally" id="V121315" title="54% like this...">
    2
    </div>
  </div>
  <a href="#121315" class="name">
  <strong class="user"><em>JBH</em></strong></a><a class="genanchor" href="#121315"> &para;</a><div class="date" title="2017-07-04 03:39"><strong>11 months ago</strong></div>
  <div class="text" id="Hcom121315">
<div class="phpcode"><code><span class="html">
I'm using PHP 7.1 and received the following warning when implementing Example #1, above:<br /><br />&nbsp; &nbsp; PHP message: PHP Warning:&nbsp; session_destroy(): Trying to destroy uninitialized session in...<br /><br />What I discovered is that clearing $_SESSION and removing the cookie destroys the session, hence the warning.&nbsp; To avoid the warning while still keeping the value of using session_destroy(), do this after everything else:<br /><br />&nbsp; &nbsp; if (session_status() == PHP_SESSION_ACTIVE) { session_destroy(); }</span>
</code></div>
  </div>
 </div>
  <div class="note" id="73045">  <div class="votes">
    <div id="Vu73045">
    <a href="/manual/vote-note.php?id=73045&amp;page=function.session-destroy&amp;vote=up" title="Vote up!" class="usernotes-voteu">up</a>
    </div>
    <div id="Vd73045">
    <a href="/manual/vote-note.php?id=73045&amp;page=function.session-destroy&amp;vote=down" title="Vote down!" class="usernotes-voted">down</a>
    </div>
    <div class="tally" id="V73045" title="46% like this...">
    -7
    </div>
  </div>
  <a href="#73045" class="name">
  <strong class="user"><em>Colin</em></strong></a><a class="genanchor" href="#73045"> &para;</a><div class="date" title="2007-02-07 01:52"><strong>11 years ago</strong></div>
  <div class="text" id="Hcom73045">
<div class="phpcode"><code><span class="html">
Note that when you are using a custom session handler, session_destroy will cause a fatal error if you have set the session destroy function used by session_set_save_handler to private.<br /><br />Example:<br />Fatal error: Call to private method Session::sessDestroy()<br /><br />where sessDestroy was the function I specified in the 5th parameter of session_set_save_handler.<br /><br />Even though it isn't all that desirable, the simple solution is to set sessDestroy to public.</span>
</code></div>
  </div>
 </div>
  <div class="note" id="119841">  <div class="votes">
    <div id="Vu119841">
    <a href="/manual/vote-note.php?id=119841&amp;page=function.session-destroy&amp;vote=up" title="Vote up!" class="usernotes-voteu">up</a>
    </div>
    <div id="Vd119841">
    <a href="/manual/vote-note.php?id=119841&amp;page=function.session-destroy&amp;vote=down" title="Vote down!" class="usernotes-voted">down</a>
    </div>
    <div class="tally" id="V119841" title="35% like this...">
    -10
    </div>
  </div>
  <a href="#119841" class="name">
  <strong class="user"><em>Gaurav</em></strong></a><a class="genanchor" href="#119841"> &para;</a><div class="date" title="2016-09-05 06:08"><strong>1 year ago</strong></div>
  <div class="text" id="Hcom119841">
<div class="phpcode"><code><span class="html">
For session_destroy() only destroy current session mean that if you specify name or change the save path of session etc&nbsp; ,it will not destroy it mean for example <br /><br />create.php <br /><br /><span class="default">&lt;?php <br />session_name</span><span class="keyword">(</span><span class="string">'testing'</span><span class="keyword">) ;<br /></span><span class="default">session_start</span><span class="keyword">() ;<br /><br /></span><span class="default">$_SESSION</span><span class="keyword">[</span><span class="string">'id'</span><span class="keyword">] = </span><span class="string">'35' </span><span class="keyword">; <br /></span><span class="default">?&gt;<br /></span><br />delete.php<br /><span class="default">&lt;?php <br />session_start</span><span class="keyword">() ; <br /><br /></span><span class="default">session_destroy</span><span class="keyword">() ;<br /></span><span class="default">?&gt;</span> <br /><br />session_destroy only delete the new session which is created by session_start(). correct way is <br /><span class="default">&lt;?php <br />session_name</span><span class="keyword">(</span><span class="string">'testing'</span><span class="keyword">) ;<br /></span><span class="default">session_start</span><span class="keyword">() ;<br /><br /></span><span class="default">session_destroy</span><span class="keyword">() ;<br /></span><span class="default">?&gt;<br /></span><br />this is also valid for if you change session.save path throught ini_set() , you have to mention in&nbsp; delete.php. <br />remember session_destroy() function destroy&nbsp; only current session not all .i hope this is worth to mention.</span>
</code></div>
  </div>
 </div>
  <div class="note" id="76832">  <div class="votes">
    <div id="Vu76832">
    <a href="/manual/vote-note.php?id=76832&amp;page=function.session-destroy&amp;vote=up" title="Vote up!" class="usernotes-voteu">up</a>
    </div>
    <div id="Vd76832">
    <a href="/manual/vote-note.php?id=76832&amp;page=function.session-destroy&amp;vote=down" title="Vote down!" class="usernotes-voted">down</a>
    </div>
    <div class="tally" id="V76832" title="29% like this...">
    -55
    </div>
  </div>
  <a href="#76832" class="name">
  <strong class="user"><em>administrator at anorhack dot com</em></strong></a><a class="genanchor" href="#76832"> &para;</a><div class="date" title="2007-08-01 07:34"><strong>10 years ago</strong></div>
  <div class="text" id="Hcom76832">
<div class="phpcode"><code><span class="html">
Destroying&nbsp; a session from a background job<br /><br />I have a thief-protection system that compares country codes from login IPs via whois. This has to run in the background as it is way too processor-hungry to be run in the browser.<br /><br />What I needed was a way to destroy the web session from the background job. For some reason, a background session_destroy APPEARS to work, but doesnt't actually destroy the web session.<br /><br />There is a work around, I set the username to NULL and the web code picks up on that, bouncing the user (thief) to a "gotcha" page where his IP is logged.<br /><br />Yes I know its nasty and dirty, but surprisingly it works.<br /><br />$sid = the session_id() of the suspicious web session, passed in $argv to the background job<br /><br />The trick is to "stuff" the $_GET array with the sid, then the session_start in the background job picks this value up (as if it were a genuine trans-sid type thing...?PHPSESSID=blah) and "connects to" the web session. All $_SESSION variable can be viewed (and CHANGED , which is how this kludge works) but for some reason (that no doubt someone will illuminate) they can't be unset...setting the particular variable to NULL works well though:<br /><br /> <br />$_GET[session_name()]=$sid;<br />session_start();<br />// prove we are getting the web session data<br />foreach($_SESSION as $k =&gt; $v) echo($k."=".$v);<br />// now kill the thief<br />$_SESSION['username']=NULL;<br />//web session variable now NULL - honestly!</span>
</code></div>
  </div>
 </div></div>

 <div class="foot"><a href="/manual/add-note.php?sect=function.session-destroy&amp;redirect=http://php.net/manual/en/function.session-destroy.php"><img src='/images/notes-add@2x.png' alt='add a note' width='12' height='12'> <small>add a note</small></a></div>
</section>    </section><!-- layout-content -->
        <aside class='layout-menu'>
    
        <ul class='parent-menu-list'>
                                    <li>
                <a href="ref.session.php">Session Functions</a>
    
                                    <ul class='child-menu-list'>
    
                          
                        <li class="">
                            <a href="function.session-abort.php" title="session_&#8203;abort">session_&#8203;abort</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-cache-expire.php" title="session_&#8203;cache_&#8203;expire">session_&#8203;cache_&#8203;expire</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-cache-limiter.php" title="session_&#8203;cache_&#8203;limiter">session_&#8203;cache_&#8203;limiter</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-commit.php" title="session_&#8203;commit">session_&#8203;commit</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-create-id.php" title="session_&#8203;create_&#8203;id">session_&#8203;create_&#8203;id</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-decode.php" title="session_&#8203;decode">session_&#8203;decode</a>
                        </li>
                          
                        <li class="current">
                            <a href="function.session-destroy.php" title="session_&#8203;destroy">session_&#8203;destroy</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-encode.php" title="session_&#8203;encode">session_&#8203;encode</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-gc.php" title="session_&#8203;gc">session_&#8203;gc</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-get-cookie-params.php" title="session_&#8203;get_&#8203;cookie_&#8203;params">session_&#8203;get_&#8203;cookie_&#8203;params</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-id.php" title="session_&#8203;id">session_&#8203;id</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-is-registered.php" title="session_&#8203;is_&#8203;registered">session_&#8203;is_&#8203;registered</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-module-name.php" title="session_&#8203;module_&#8203;name">session_&#8203;module_&#8203;name</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-name.php" title="session_&#8203;name">session_&#8203;name</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-regenerate-id.php" title="session_&#8203;regenerate_&#8203;id">session_&#8203;regenerate_&#8203;id</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-register-shutdown.php" title="session_&#8203;register_&#8203;shutdown">session_&#8203;register_&#8203;shutdown</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-register.php" title="session_&#8203;register">session_&#8203;register</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-reset.php" title="session_&#8203;reset">session_&#8203;reset</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-save-path.php" title="session_&#8203;save_&#8203;path">session_&#8203;save_&#8203;path</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-set-cookie-params.php" title="session_&#8203;set_&#8203;cookie_&#8203;params">session_&#8203;set_&#8203;cookie_&#8203;params</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-set-save-handler.php" title="session_&#8203;set_&#8203;save_&#8203;handler">session_&#8203;set_&#8203;save_&#8203;handler</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-start.php" title="session_&#8203;start">session_&#8203;start</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-status.php" title="session_&#8203;status">session_&#8203;status</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-unregister.php" title="session_&#8203;unregister">session_&#8203;unregister</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-unset.php" title="session_&#8203;unset">session_&#8203;unset</a>
                        </li>
                          
                        <li class="">
                            <a href="function.session-write-close.php" title="session_&#8203;write_&#8203;close">session_&#8203;write_&#8203;close</a>
                        </li>
                            
                    </ul>
                    
            </li>
                        
                    </ul>
    </aside>


  </div><!-- layout -->
         
  <footer>
    <div class="container footer-content">
      <div class="row-fluid">
      <ul class="footmenu">
        <li><a href="/copyright.php">Copyright &copy; 2001-2018 The PHP Group</a></li>
        <li><a href="/my.php">My PHP.net</a></li>
        <li><a href="/contact.php">Contact</a></li>
        <li><a href="/sites.php">Other PHP.net sites</a></li>
        <li><a href="/mirrors.php">Mirror sites</a></li>
        <li><a href="/privacy.php">Privacy policy</a></li>
      </ul>
      </div>
    </div>
  </footer>

    
 <!-- External and third party libraries. -->
 <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/ext/modernizr.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/ext/hogan-2.0.0.min.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/ext/typeahead.min.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/ext/mousetrap.min.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1421837618&amp;f=/js/search.js"></script>
<script type="text/javascript" src="http://php.net/cached.php?t=1516300802&amp;f=/js/common.js"></script>

<a id="toTop" href="javascript:;"><span id="toTopHover"></span><img width="40" height="40" alt="To Top" src="/images/to-top@2x.png"></a>

</body>
</html>

