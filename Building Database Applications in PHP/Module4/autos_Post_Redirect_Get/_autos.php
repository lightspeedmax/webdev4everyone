<?php
require_once "pdo.php";

// Demand a GET parameter
if ( ! isset($_GET['name']) || strlen($_GET['name']) < 1  ) {
    die('Name parameter missing');
}

// If the user requested logout go back to index.php
if ( isset($_POST['logout']) ) {
    header('Location: index.php');
    return;
}

$isDataSet = FALSE;
$isDataNumeric = FALSE;
$isMakeValid = FALSE;
$isRecordInserted = FALSE;

if ( isset($_POST['Add']) )
{
  if ( isset($_POST['make']) &&
       isset($_POST['year']) &&
       isset($_POST['mileage']))
  {
    $isDataSet = TRUE;
  }

  if (strlen($_POST['make']) > 1)
  //if (strlen($_POST['make']) < 1)
  {
    $isMakeValid = TRUE;
    if ( is_numeric($_POST['year']) &&
         is_numeric($_POST['mileage']))
    {
      $isDataNumeric = TRUE;
    }
    else
    {
      echo('<p style="color: red;">'.htmlentities("Mileage and year must be numeric")."</p>\n");
    //  break;
    }
  }
  else
  {
    echo('<p style="color: red;">'.htmlentities("Make is required")."</p>\n");
  }

  if(($isDataSet === TRUE) &&
     ($isDataNumeric === TRUE) &&
     ($isMakeValid === TRUE))
  {
    $sql = "INSERT INTO autos (make, year, mileage)
              VALUES (:make, :year, :mileage)";
    //echo("<pre>\n".$sql."\n</pre>\n");
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(
        ':make' => htmlentities($_POST['make']),
        ':year' => $_POST['year'],
        ':mileage' => $_POST['mileage']));
    $isRecordInserted = TRUE;
  }
}
else
{
    echo('<p style="color: blue;">'.htmlentities("...")."</p>\n");
}

/*
    if ( isset($_POST['Make']) &&
         isset($_POST['Year']) &&
         isset($_POST['Mileage'])) {

           if ( is_numeric($_POST['Year']) &&
                is_numeric($_POST['Mileage'])) {

              $sql = "INSERT INTO autos (make, year, mileage)
                        VALUES (:make, :year, :mileage)";
              //echo("<pre>\n".$sql."\n</pre>\n");
              $stmt = $pdo->prepare($sql);
              $stmt->execute(array(
                  ':make' => $_POST['Make'],
                  ':year' => $_POST['Year'],
                  ':mileage' => $_POST['Mileage']));
           } else {
             // code...

             echo('<p style="color: red;">'.htmlentities("Mileage and year must be numeric")."</p>\n");
           }
      }
  }
*/


?>
<!DOCTYPE html>
<html>
<head>
<title>>Tracking Autos for Craig Mullins</title>
<?php //require_once "bootstrap.php"; ?>
</head>
<body>
<div class="container">
<h1>Tracking Autos for Craig Mullins</h1>
<?php
if ($isRecordInserted === TRUE)
{
  echo('<p style="color: green;">'.htmlentities("Record inserted")."</p>\n");
}
else if ( isset($_REQUEST['name']) )
{
    echo "<p>Welcome: ";
    echo htmlentities($_REQUEST['name']);
    echo "</p>\n";
}


?>
<form method="post">
<p>Make:
<input type="text" name="make" size="60"/></p>
<p>Year:
<input type="text" name="year"/></p>
<p>Mileage:
<input type="text" name="mileage"/></p>
<input type="submit" name="Add" value="Add">
<input type="submit" name="logout" value="Logout">
</form>
<h2>Automobiles</h2>
<pre>
<?php
$stmt = $pdo->query("SELECT make, year, mileage FROM autos");
//var_dump($stmt);
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
//echo "<p>Querying for data... </p>";
?>
<html>
<head></head><body><table border="1">
<?php
if($rows !== FALSE ) {
  foreach ( $rows as $row ) {
      echo "<tr><td>";
      echo($row['make']);
      echo("</td><td>");
      echo($row['year']);
      echo("</td><td>");
      echo($row['mileage']);
      echo("</td></tr>\n");

      echo "<tr><td>";

  }
}
?>
</table>

</pre>

</div>
</body>
</html>
