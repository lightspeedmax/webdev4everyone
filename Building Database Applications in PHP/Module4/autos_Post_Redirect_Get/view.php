<?php
require_once "pdo.php";
    session_start();

    if ( ! isset($_SESSION["account"]) ) {
      die('Not logged in');
    }



//----------------View------------------------
?>
<html>
<head>
  <?php// require_once "bootstrap.php"; ?>
  <title>>Tracking Autos for Craig Mullins</title>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

</head>
<body style="font-family: sans-serif;">
<h1>Tracking Autos for Craig Mullins</h1>
<h2>Automobiles</h2>
<p>
<?php
    if ( isset($_SESSION["success"]) ) {
        echo('<p style="color: green;">'.htmlentities($_SESSION['success'])."</p>\n");
        unset($_SESSION["success"]);
    }
?>

<?php
// Make the Query to get Vehicles
$stmt = $pdo->query("SELECT make, year, mileage FROM autos");
//var_dump($stmt);
$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
//echo "<p>Querying for data... </p>";
?>


<?php
echo "<ul>";
if($rows !== FALSE ) {
  foreach ( $rows as $row ) {
      echo "<li>";
      echo($row['year']);
      echo(" ");
      echo($row['make']);
      echo(" /");
      echo($row['mileage']);
      echo("</li>\n");

  }
}
echo("</ul>\n");
?>


</p>
      <p> <a href="add.php">Add New</a> | <a href="logout.php">Log Out</a></p>

</body>
</html>
