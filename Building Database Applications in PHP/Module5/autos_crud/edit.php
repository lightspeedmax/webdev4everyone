<?php
require_once "pdo.php";
session_start();

if ( ! isset($_SESSION["account"]) ) {
  die('ACCESS DENIED');
}

// Guardian: Make sure that user_id is present
if ( ! isset($_GET['autos_id']) ) {
  $_SESSION['error'] = "Missing user_id";
  header('Location: index.php');
  return;
}

// If the user requested logout go back to index.php
if ( isset($_POST['cancel']) ) {
    header('Location: index.php');
    return;
}

//  if { isset($_POST['Add']) )
$isDataSet = FALSE;
$isDataNumeric = FALSE;
$isMakeValid = FALSE;
$isRecordInserted = FALSE;

if ( isset($_POST['autos_id']) &&
     isset($_POST['make']) &&
     isset($_POST['model']) &&
     isset($_POST['year']) &&
     isset($_POST['mileage']) )
{
//  if ( isset($_POST['make']) &&
//       isset($_POST['model']) &&
//       isset($_POST['year']) &&
//       isset($_POST['mileage']))
  {
    $isDataSet = TRUE;
  }

  if ((strlen($_POST['make']) > 1) &&
      (strlen($_POST['model']) > 1))
  //if (strlen($_POST['make']) < 1)
  {
    $isMakeValid = TRUE;
    if ( is_numeric($_POST['year']) &&
         is_numeric($_POST['mileage']))
    {
      $isDataNumeric = TRUE;
    }
    else
    {
      //echo('<p style="color: red;">'.htmlentities("Mileage and year must be numeric")."</p>\n");
      $_SESSION["error"] = "Mileage and year must be numeric";
      header( "Location: edit.php?autos_id=".$_POST['autos_id'] ) ;
      return;
    //  break;
    }
  }
  else
  {
    //echo('<p style="color: red;">'.htmlentities("Make is required")."</p>\n");
    $_SESSION["error"] = "Make is required";
    header( "Location: edit.php?autos_id=".$_POST['autos_id'] ) ;
    return;
  }

  if(($isDataSet === TRUE) &&
     ($isDataNumeric === TRUE) &&
     ($isMakeValid === TRUE))
  {
    $sql = "UPDATE autos SET make =:make,
                            model =:make,
                            year =:year,
                            mileage =:mileage
              WHERE autos_id = :autos_id";
    //echo("<pre>\n".$sql."\n</pre>\n");
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(
        ':make' => htmlentities($_POST['make']),
        ':model' => htmlentities($_POST['model']),
        ':year' => $_POST['year'],
        ':mileage' => $_POST['mileage'],
        ':autos_id' => $_POST['autos_id']));
    $isRecordInserted = TRUE;

    $_SESSION["success"] = "Record updated";
    header( 'Location: index.php' ) ;
    return;

  }
}
else
{
    echo('<p style="color: blue;">'.htmlentities("...")."</p>\n");
}

$stmt = $pdo->prepare("SELECT * FROM autos where autos_id = :xyz");
$stmt->execute(array(":xyz" => $_GET['autos_id']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if ( $row === false ) {
    $_SESSION['error'] = 'Bad value';
    header( 'Location: index.php' ) ;
    return;
}

// Flash pattern
//if ( isset($_SESSION['error']) ) {
//    echo '<p style="color:red">'.$_SESSION['error']."</p>\n";
//    unset($_SESSION['error']);
//}

$n = htmlentities($row['make']);
$e = htmlentities($row['model']);
$p = htmlentities($row['year']);
$q = htmlentities($row['mileage']);
$autos_id = $row['autos_id'];

//----------------View------------------------
?>
<html>
<head>
  <?php //require_once "bootstrap.php"; ?>
  <title>>Tracking Autos for Craig Mullins</title>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

</head>
<body style="font-family: sans-serif;">
<h1>Tracking Autos for Craig Mullins</h1>
<?php
/* Valid if a user has actually logged in */
    if ( isset($_SESSION["error"]) ) {
        echo('<p style="color:red">'.$_SESSION["error"]."</p>\n");
        unset($_SESSION["error"]);
    }
?>
<form method="post">
<p>Make:
<input type="text" name="make" value="<?= $n ?>" size="40"/></p>
<p>Model:
<input type="text" name="model" value="<?= $e ?>" size="40"/></p>
<p>Year:
<input type="text" name="year" value="<?= $p ?>" /></p>
<p>Mileage:
<input type="text" name="mileage" value="<?= $q ?>" /></p>
<input type="hidden" name="autos_id" value="<?= $autos_id ?>">
<input type="submit" name="edit" value="Save"/>
<input type="submit" name="cancel" value="Cancel">
</form>

</body>
</html>
