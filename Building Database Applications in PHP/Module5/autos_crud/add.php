<?php
require_once "pdo.php";
    session_start();

    if ( ! isset($_SESSION["account"]) ) {
      die('ACCESS DENIED');
    }

    // If the user requested logout go back to index.php
    if ( isset($_POST['cancel']) ) {
        header('Location: index.php');
        return;
    }

  //  if { isset($_POST['Add']) )
  $isDataSet = FALSE;
  $isDataNumeric = FALSE;
  $isMakeValid = FALSE;
  $isRecordInserted = FALSE;

  if ( isset($_POST['add']) )
  {
    if ( isset($_POST['make']) &&
         isset($_POST['model']) &&
         isset($_POST['year']) &&
         isset($_POST['mileage']))
    {
      $isDataSet = TRUE;
    }

    if ((strlen($_POST['make']) > 1) &&
        (strlen($_POST['model']) > 1))
    //if (strlen($_POST['make']) < 1)
    {
      $isMakeValid = TRUE;
      if ( is_numeric($_POST['year']) &&
           is_numeric($_POST['mileage']))
      {
        $isDataNumeric = TRUE;
      }
      else
      {
        //echo('<p style="color: red;">'.htmlentities("Mileage and year must be numeric")."</p>\n");
        $_SESSION["error"] = "Mileage and year must be numeric";
        header( 'Location: add.php' ) ;
        return;
      //  break;
      }
    }
    else
    {
      //echo('<p style="color: red;">'.htmlentities("Make is required")."</p>\n");
      $_SESSION["error"] = "All values are required";//"Make is required";
      header( 'Location: add.php' ) ;
      return;
    }

    if(($isDataSet === TRUE) &&
       ($isDataNumeric === TRUE) &&
       ($isMakeValid === TRUE))
    {
      $sql = "INSERT INTO autos (make, model,year, mileage)
                VALUES (:make, :model,:year, :mileage)";
      //echo("<pre>\n".$sql."\n</pre>\n");
      $stmt = $pdo->prepare($sql);
      $stmt->execute(array(
          ':make' => htmlentities($_POST['make']),
          ':model' => htmlentities($_POST['model']),
          ':year' => $_POST['year'],
          ':mileage' => $_POST['mileage']));
      $isRecordInserted = TRUE;

      $_SESSION["success"] = "added";//"Record inserted";
      header( 'Location: index.php' ) ;
      return;

    }
  }
  else
  {
      echo('<p style="color: blue;">'.htmlentities("...")."</p>\n");
  }



//----------------View------------------------
?>
<html>
<head>
  <?php //require_once "bootstrap.php"; ?>
  <title>>Tracking Autos for Craig Mullins</title>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

</head>
<body style="font-family: sans-serif;">
<h1>Tracking Autos for Craig Mullins</h1>
<?php
/* Valid if a user has actually logged in */
    if ( isset($_SESSION["error"]) ) {
        echo('<p style="color:red">'.$_SESSION["error"]."</p>\n");
        unset($_SESSION["error"]);
    }
?>
<form method="post">
<p>Make:
<input type="text" name="make" size="40"/></p>
<p>Model:
<input type="text" name="model" size="40"/></p>
<p>Year:
<input type="text" name="year"/></p>
<p>Mileage:
<input type="text" name="mileage"/></p>
<input type="submit" name="add" value="Add">
<input type="submit" name="cancel" value="Cancel">
</form>

</body>
</html>
