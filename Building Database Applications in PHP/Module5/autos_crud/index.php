<?php
require_once "pdo.php";
    session_start();

$isLoggedIn = FALSE;
    if ( isset($_SESSION["account"]) ) {
      $isLoggedIn = TRUE;
    }



//----------------View------------------------
?>

<!DOCTYPE html>
<html>
<head>
<title>Craig Mullins Tracking Autos</title>
<?php require_once "bootstrap.php"; ?>
</head>
<body>
<div class="container">
<h1>Welcome to Automobile Database</h1>
<?php
  if($isLoggedIn === FALSE)
  {
    echo ('<p> <a href="login.php">Please log in</a> </p>');
    echo ('<p> Attempt to <a href="add.php">add data</a> without logging in</p>');
  }
  else
  {
    if ( isset($_SESSION["success"]) ) {
        echo('<p style="color: green;">'.htmlentities($_SESSION['success'])."</p>\n");
        unset($_SESSION["success"]);
    }

    if ( isset($_SESSION['error']) ) {
        echo '<p style="color:red">'.$_SESSION['error']."</p>\n";
        unset($_SESSION['error']);
    }

    // Make the Query to get Vehicles
    $stmt = $pdo->query("SELECT make, model, year, mileage, autos_id FROM autos");
    //var_dump($stmt);
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    //echo "<p>Querying for data... </p>";
    $rowcounter = 0;

    //----------------------------------------------------------

    echo('<table border="1">'."\n");

    echo "<tr><td>";
    echo('<b>'."Make".'</b>');
    echo("</td><td>");
    echo('<b>'."Model".'</b>');
    echo("</td><td>");
    echo('<b>'."Year".'</b>');
    echo("</td><td>");
    echo('<b>'."Mileage".'</b>');
    echo("</td><td>");
    echo('<b>'."Action".'</b>');

    echo("</td></tr>\n");

    foreach ( $rows as $row)  {
        $rowcounter++;
        echo "<tr><td>";
        echo(htmlentities($row['make']));
        echo("</td><td>");
        echo(htmlentities($row['model']));
        echo("</td><td>");
        echo(htmlentities($row['year']));
        echo("</td><td>");
        echo(htmlentities($row['mileage']));
        echo("</td><td>");
        echo('<a href="edit.php?autos_id='.$row['autos_id'].'">Edit</a> / ');
        echo('<a href="delete.php?autos_id='.$row['autos_id'].'">Delete</a>');
        echo("</td></tr>\n");
    }
    echo('</table>'."\n");



    //----------------------------------------------------------

    if($rowcounter == 0)
    {
      echo ("<p> No rows found </p>");
      // code...
    }

    echo ('<p> <a href="add.php">Add New Entry</a> </p>');
    echo ('<p> <a href="logout.php">Logout</a></p>');

  }

//  <p>
//  Attempt to
//  <a href="add.php">add data</a> without logging in
//  </p>

?>


</div>
</body>
</html>
