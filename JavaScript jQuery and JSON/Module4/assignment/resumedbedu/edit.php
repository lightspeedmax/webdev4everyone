<?php
require_once "pdo.php";
require_once "util.php";
session_start();

die_if_not_logged_in();


// Guardian: Make sure that user_id is present
if ( ! isset($_GET['profile_id']) ) {
  $_SESSION['error'] = "Missing profile_id";
  header('Location: index.php');
  return;
}

// If the user requested logout go back to index.php
if ( isset($_POST['cancel']) ) {
    header('Location: index.php');
    return;
}

//  if { isset($_POST['Add']) )
$isDataSet = FALSE;
$isDataNumeric = FALSE;
$isMakeValid = FALSE;
$isRecordInserted = FALSE;
$isPositionsValid = FALSE;
$isEducationValid = FALSE;

if ( isset($_POST['first_name']) &&
     isset($_POST['last_name']) &&
     isset($_POST['headline']) &&
     isset($_POST['summary']) &&
     isset($_POST['email']))
{
//  if ( isset($_POST['make']) &&
//       isset($_POST['model']) &&
//       isset($_POST['year']) &&
//       isset($_POST['mileage']))
  {
    $isDataSet = TRUE;
  }

  if ((strlen($_POST['first_name']) > 1) &&
      (strlen($_POST['last_name']) > 1) &&
      (strlen($_POST['email']) > 1) &&
      (strlen($_POST['headline']) > 1) &&
      (strlen($_POST['summary']) > 1)
      )
  //if (strlen($_POST['make']) < 1)
  {
    $substr = "@";
    if(strpos($_POST['email'], $substr) !== false)
    {
      $isMakeValid = TRUE;
    }
    else
    {
      $_SESSION["error"] = "Bad email Address";
      header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
      return;
    }

    $PositionValidationCheck = validatePos();
    if ($PositionValidationCheck === TRUE)
    {
      $isPositionsValid = TRUE;
      $isDataNumeric = TRUE;
    }
    else
    {
      //echo('<p style="color: red;">'.htmlentities("Mileage and year must be numeric")."</p>\n");
      $_SESSION["error"] = $PositionValidationCheck;
      header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
      return;
    }

    $EducationValidationCheck = validateEdu();
    if ($EducationValidationCheck === TRUE)
    {
      $isEducationValid = TRUE;
      $isDataNumeric = TRUE;
    }
    else
    {
      //echo('<p style="color: red;">'.htmlentities("Mileage and year must be numeric")."</p>\n");
      $_SESSION["error"] = $EducationValidationCheck;
      header( 'Location: add.php' ) ;//header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
      return;
    }
  }
  else
  {
    //echo('<p style="color: red;">'.htmlentities("Make is required")."</p>\n");
    $_SESSION["error"] = "All values are required";
    header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
    return;
  }

//  echo "<pre>"; print_r($_POST) ;  echo "</pre>";

  if(($isDataSet === TRUE) &&
     ($isDataNumeric === TRUE) &&
     ($isMakeValid === TRUE) &&
     ($isPositionsValid === TRUE) &&
     ($isEducationValid === TRUE)
     )
  {
    $sql = "UPDATE profile SET first_name =:first_name, last_name =:last_name,
                            email =:email,
                            headline =:headline,
                            summary =:summary
              WHERE profile_id = :profile_id";


    //echo("<pre>\n".$sql."\n</pre>\n");
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(
        ':first_name' => htmlentities($_POST['first_name']),
        ':last_name' => htmlentities($_POST['last_name']),
        ':email' => htmlentities($_POST['email']),
        ':headline' => htmlentities($_POST['headline']),
        ':summary' => htmlentities($_POST['summary']),
        ':profile_id' => $_POST['profile_id']));

    $profile_id = $_POST['profile_id'];

    // Clear out the old position entries
    $stmtDelete = $pdo->prepare('DELETE FROM position WHERE profile_id=:pid');
    $stmtDelete->execute(array( ':pid' => $profile_id));
    PersistPositions($pdo,$profile_id);
    $isRecordInserted = TRUE;

    // Clear out the old Education entries
    $stmtEduDelete = $pdo->prepare('DELETE FROM education WHERE profile_id=:pid');
    $stmtEduDelete->execute(array( ':pid' => $profile_id));
    PersistEducation($pdo,$profile_id);


    $_SESSION["success"] = "Record updated Profile ID".$profile_id." Rank Count:".$rank;

    header( 'Location: index.php' ) ;
    return;

    echo "<pre>"; print_r($_POST) ;  echo "</pre>";

  }
  else
  {
      echo "<pre>"; print_r($_POST) ;  echo "</pre>";

  }
}
else
{
    echo('<p style="color: blue;">'.htmlentities("...")."</p>\n");

    // For testing:
    var_dump($_POST);

}
$profile_id = $_GET['profile_id'];

$stmt = $pdo->prepare("SELECT * FROM profile where profile_id = :xyz");
$stmt->execute(array(":xyz" => $profile_id));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if ( $row === false ) {
    $_SESSION['error'] = 'Bad value';
    header( 'Location: index.php' ) ;
    return;
}

$stmtPosition = $pdo->prepare("SELECT year,description FROM position where profile_id = :xyz");
$stmtPosition ->execute(array(":xyz" => $profile_id));

$rowPositions = $stmtPosition->fetchAll(PDO::FETCH_ASSOC);

$positionRowcounter = 0;
if ( $rowPositions !== false )
{
  foreach ( $rowPositions as $rowsToCount)
  {
//      echo("Counting: ".$rowsToCount);
      $positionRowcounter++;
  }
}

// education
$stmtEducation = $pdo->prepare("SELECT education.year,institution.name
                                FROM education, institution
                                where profile_id = :xyz
                                AND education.institution_id = institution.institution_id");
$stmtEducation ->execute(array(":xyz" => $profile_id));
$rowEducation = $stmtEducation->fetchAll(PDO::FETCH_ASSOC);
$educationCount = Count($rowEducation);
echo("Education Count: ".$educationCount.'<br>');
echo("Position Count: ".$positionRowcounter);

//-----------------------------
$fn = htmlentities($row['first_name']);
$ln = htmlentities($row['last_name']);
$email = htmlentities($row['email']);
$head = htmlentities($row['headline']);
$sum = htmlentities($row['summary']);
//$profile_id = $row['profile_id'];

//----------------View------------------------
?>
<html>
<head>
  <?php //require_once "bootstrap.php"; ?>
  <title>Craig Mullins Resume Profile</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
</head>
<body style="font-family: sans-serif;">

<?php
echo ('<h1>Editing Profile for Me '.$fn.'</h1>');
/* Valid if a user has actually logged in */
    if ( isset($_SESSION["error"]) ) {
        echo('<p style="color:red">'.$_SESSION["error"]."</p>\n");
        unset($_SESSION["error"]);
    }
?>

<form method="post">
<p>First Name:
<input type="text" name="first_name" value="<?= $fn ?>" size="60"/></p>
<p>Last Name:
<input type="text" name="last_name" value="<?= $ln ?>" size="60"/></p>
<p>Email:
<input type="text" name="email" value="<?= $email ?>" size="40"/></p>
<p>Headline:
<input type="text" name="headline" value="<?= $head ?>" size="40"/></p>
<p>Summary:
<br>
<textarea name="summary" rows="4" cols="60"><?=$sum ?></textarea>
<br>
<input type="hidden" name="profile_id" value="<?=$profile_id?>">
<p>
Education: <input type="submit" id="addEdu" value="+">
<div id="edu_fields">
  <?php
    if($educationCount != 0)
    {
      $rowcounter=1;
      foreach ( $rowEducation as $row)
      {
        $year = htmlentities($row['year']);
        $name = htmlentities($row['name']);

        echo('<div id="eduposition'.$rowcounter.'">'."\n");
        echo('<p>Year: <input type="text" name="edu_year'.$rowcounter.'" value="'.$year.'" />'."\n");
        echo('<input type="button" value="-" onclick="$(\'#eduposition'.$rowcounter.'\').remove();return false;">'."\n");
        echo('</p>'."\n");

        echo('<p>School: <input type="text" size="80" name="edu_school'.$rowcounter.'" class="school" value="'.$name.'" />'."\n");
        echo('</p>'."\n");
        echo('</div>'."\n");
        $rowcounter++;
      }
    }
  ?>
</div>
</p>
<p>Position: <input type="submit" id="addPos" value="+">
<div id="position_fields">
<?php
  if($positionRowcounter != 0)
  {
    $rowcounter=1;
    foreach ( $rowPositions as $row)
    {
      $year = htmlentities($row['year']);
      $description = htmlentities($row['description']);

      echo('<div id="position'.$rowcounter.'">'."\n");
      echo('<p>Year: <input type="text" name="year'.$rowcounter.'" value="'.$year.'" />'."\n");
      echo('<input type="button" value="-" onclick="$(\'#position'.$rowcounter.'\').remove();return false;">'."\n");
      echo('</p>'."\n");

      echo('<textarea name="desc'.$rowcounter.'" rows="8" cols="80">'."\n");
      echo($description );
      echo('</textarea>'."\n");
      echo('</div>'."\n");
      $rowcounter++;
    }
  }
?>
</div>
</p>

<input type="submit" name="edit" value="Save"/>
<input type="submit" name="cancel" value="Cancel">
</form>

<script>
countPos = 0;
countEduPos = 0;


// http://stackoverflow.com/questions/17650776/add-remove-html-inside-div-using-javascript
$(document).ready(function(){
    window.console && console.log('Document ready called');

    // Once document has loaded, we need to somehow check what the last position
    // rank number is
    var i;
    for (i = 0; i <= 9; i++) {
      if(document.getElementById('position'+i) !== null)
      {
        window.console && console.log('Found ID tag position'+i);
        countPos = i+1;
      }

      if(document.getElementById('eduposition'+i) !== null)
      {
        window.console && console.log('Found ID tag eduposition'+i);
        countEduPos = i+1;
      }
    }

    window.console && console.log('Current position Count is '+countPos);



    // Now register a callback event
    $('#addPos').click(function(event){
        // http://api.jquery.com/event.preventdefault/
        event.preventDefault();
        if ( countPos >= 10 ) {
            alert("Maximum of nine position entries exceeded");
            return;
        }
        countPos++;
        window.console && console.log("Adding position "+countPos);
        $('#position_fields').append(
            '<div id="position'+countPos+'"> \
            <p>Year: <input type="text" name="year'+countPos+'" value="" /> \
            <input type="button" value="-" \
                onclick="$(\'#position'+countPos+'\').remove();return false;"></p> \
            <textarea name="desc'+countPos+'" rows="8" cols="80"></textarea>\
            </div>');
    });

    $('#addEdu').click(function(event){
        // http://api.jquery.com/event.preventdefault/
        event.preventDefault();
        if ( countEduPos >= 9 ) {
            alert("Maximum of nine position entries exceeded");
            return;
        }
        countEduPos++;
        window.console && console.log("Adding position "+countEduPos);
        $('#edu_fields').append(
            '<div id="eduposition'+countEduPos+'"> \
            <p>Year: <input type="text" name="edu_year'+countEduPos+'" value="" /> \
            <input type="button" value="-" \
                onclick="$(\'#eduposition'+countEduPos+'\').remove();return false;"></p> \
            <p>School: <input type="text" size="80" name="edu_school'+countEduPos+'" class="school" value="" /> \
            </div>');

        $('.school').autocomplete({
                source: "school.php"
            });
    });

    $('.school').autocomplete({
            source: "school.php"
        });

});
</script>

</body>
</html>
