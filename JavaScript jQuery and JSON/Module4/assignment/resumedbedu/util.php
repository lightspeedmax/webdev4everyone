<?php
require_once "pdo.php";
//session_start();
function validate_logged_in()
{
  if ( isset($_SESSION["account"]) ) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

function die_if_not_logged_in ()
{
  if ( ! isset($_SESSION["account"]) )
  {
    die('ACCESS DENIED');
  }
}

function flashMessages()
{
  if ( isset($_SESSION['error']) ) {
      echo '<p style="color:red">'.$_SESSION['error']."</p>\n";
      unset($_SESSION['error']);
  }
  if ( isset($_SESSION['success']) ) {
      echo '<p style="color:green">'.$_SESSION['success']."</p>\n";
      unset($_SESSION['success']);
  }
}
/*
function add ()
{
  var $sql = "INSERT INTO profile ( user_id, first_name, last_name, email, headline, summary)
            VALUES (:user_id, :first_name, :last_name, :email, :headline, :summary)";

  var $stmt = $pdo->prepare($sql);
  $stmt->execute(array(
      ':first_name' => htmlentities($_POST['first_name']),
      ':last_name'  => htmlentities($_POST['last_name']),
      ':email'      => htmlentities($_POST['email']),
      ':headline'   => htmlentities($_POST['headline']),
      ':summary'    => htmlentities($_POST['summary']),
      ':user_id'    => $_SESSION['account']));

  var $profile_id = $pdo->lastInsertId();

}
*/

function validatePos()
{
//  return "Never Valid";
  for($i=1; $i<=9; $i++)
  {
    if ( ! isset($_POST['year'.$i]) ) continue;
    if ( ! isset($_POST['desc'.$i]) ) continue;

     $year = $_POST['year'.$i];
     $desc = $_POST['desc'.$i];

    if ( strlen($year) == 0 || strlen($desc) == 0 )
    {
      return "All fields are required";
    }

    if ( ! is_numeric($year) )
    {
      return "Position year must be numeric";
    }
  }
  return true;
}


function validateEdu()
{
//  return "Never Valid";
  for($i=1; $i<=9; $i++) {
    if ( ! isset($_POST['edu_year'.$i]) ) continue;
    if ( ! isset($_POST['edu_school'.$i]) ) continue;

     $year = $_POST['edu_year'.$i];
     $uniV = $_POST['edu_school'.$i];

    if ( strlen($year) == 0 || strlen($uniV) == 0 ) {
      return "All fields are required";
    }

    if ( ! is_numeric($year) ) {
      return "Position year must be numeric";
    }
  }
  return true;
}

function PesistResume($pdo)
{
   $sql = "INSERT INTO profile ( user_id, first_name, last_name, email, headline, summary)
            VALUES (:user_id, :first_name, :last_name, :email, :headline, :summary)";

   $stmt = $pdo->prepare($sql);
  $stmt->execute(array(
      ':first_name' => htmlentities($_POST['first_name']),
      ':last_name'  => htmlentities($_POST['last_name']),
      ':email'      => htmlentities($_POST['email']),
      ':headline'   => htmlentities($_POST['headline']),
      ':summary'    => htmlentities($_POST['summary']),
      ':user_id'    => $_SESSION['account']));
}

function PersistPositions($pdo,$profile_id)
{
   $rank = 1;

// Now add the Positions to the postions table

  for($i=1; $i<=9; $i++)
  {
    if ( ! isset($_POST['year'.$i]) ) continue;
    if ( ! isset($_POST['desc'.$i]) ) continue;

    $year = $_POST['year'.$i];
    $desc = $_POST['desc'.$i];

    $sqlPosition = "INSERT INTO position (profile_id, rank, year, description)
                            VALUES ( :pid, :rank, :year, :desc)";

    $stmtPosition = $pdo->prepare($sqlPosition);

    $stmtPosition->execute(array(
      ':pid' => $profile_id,
      ':rank' => $rank,
      ':year' => $year,
      ':desc' => $desc)
    );
    $rank++;
  }
}

function PersistEducation($pdo,$profile_id)
{
  $rank = 1;
// Now add the Positions to the postions table
 for($i=1; $i<=9; $i++)
 {
   $institution_id = false;
   if ( ! isset($_POST['edu_year'.$i]) ) continue;
   if ( ! isset($_POST['edu_school'.$i]) ) continue;

   $year = $_POST['edu_year'.$i];
   $school = $_POST['edu_school'.$i];

   // Look up the school if it exists
   $sqlInstitution = "SELECT institution_id FROM institution WHERE  name = :name";
   $stmtInst = $pdo->prepare($sqlInstitution);
   $stmtInst->execute($arrayName = array(':name' => $school));
   $row = $stmtInst->fetch(PDO::FETCH_ASSOC);
   if($row != FALSE)
   {
     $institution_id = $row['institution_id'];
   }

   //if there is no institution then insert it.
   if($institution_id == false)
   {
     $sqlInsertInstitution = "INSERT INTO institution (name) VALUES (:name)";
     $stmtInsertInst = $pdo->prepare($sqlInsertInstitution);
     $stmtInsertInst->execute(array(
       ':name' => $school)
     );
     $institution_id = $pdo->lastInsertId();

     //echo ("Institution School: ".$school." ID: ".$institution_id." Saved...");
   }
   else {
     // code...
     //var_dump($institution_id);
   }
   try {

     $sqlInsertEducation = "INSERT INTO education (profile_id, institution_id, rank, year)
                             VALUES ( :pid, :inst,:rank, :year)";

     $stmtPosition = $pdo->prepare($sqlInsertEducation);

     $stmtPosition->execute(array(
       ':pid' => $profile_id,
       ':inst' => $institution_id,
       ':rank' => $rank,
       ':year' => $year)
     );

   } catch (\Exception $e)
   {
     // There primary key is a combination of the institution_id and profile_id
     // So this will fail on the database side. Not quite sure what the requirement is here.


   }
   $rank++;
 }
}

function GetPositions($pdo,$profile_id)
{
  $stmtPosition = $pdo->prepare("SELECT year,description FROM position where profile_id = :xyz");
  $stmtPosition ->execute(array(":xyz" => $profile_id));
  $rowPositions = $stmtPosition->fetchAll(PDO::FETCH_ASSOC);
  return $rowPositions;
}

?>
