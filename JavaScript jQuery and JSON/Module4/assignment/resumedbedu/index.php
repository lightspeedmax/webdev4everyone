<?php
require_once "pdo.php";
require_once "util.php";
    session_start();

$isLoggedIn = validate_logged_in();

//----------------View------------------------
?>

<!DOCTYPE html>
<html>
<head>
<title>Craig Mullins Resume Registry</title>
<?php require_once "bootstrap.php"; ?>
</head>
<body>
<div class="container">
<h1>Welcome to the Resume Registry</h1>
<?php

//  $this_user_id = $_SESSION["account"];

  $sql = "SELECT profile_id,first_name, headline FROM profile ";//WHERE user_id = :user_id";
  // Make the Query to get Vehicles
  $stmt = $pdo->query($sql);

  //var_dump($stmt);
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  //echo "<p>Querying for data... </p>";
  $rowcounter = 0;

  // This is crude way to check if array has VALUES

  foreach ( $rows as $row)
  {
      $rowcounter++;
  //    break;
  }

//  echo('<b>'."$rowcounter".'</b>');


  if($isLoggedIn === FALSE)
  {
    echo ('<p> <a href="login.php">Please log in</a> </p>');

    if($rowcounter != 0)
    {
      echo('<table border="1">'."\n");
      echo "<tr><td>";
      echo('<b>'."Name".'</b>');
      echo("</td><td>");
      echo('<b>'."Headline".'</b>');
      echo("</td></tr>\n");

      foreach ( $rows as $row)  {
          $rowcounter++;
          echo "<tr><td>";
          echo(htmlentities($row['first_name']));
          echo("</td><td>");
          echo(htmlentities($row['headline']));
          echo("</td><td>");

  //        echo('<a href="edit.php?autos_id='.$row['autos_id'].'">Edit</a> / ');
  //        echo('<a href="delete.php?autos_id='.$row['autos_id'].'">Delete</a>');
  //        echo("</td></tr>\n");
      }
      echo('</table>'."\n");
    } // End if($rowcounter != 0)
  }
  else // if($isLoggedIn === FALSE)
  {
    // If Logged

    flashMessages();

    echo ('<p> <a href="logout.php">Logout</a></p>');


    if($rowcounter != 0)
    {
      echo('<table border="1">'."\n");
      echo "<tr><td>";
      echo('<b>'."Name".'</b>');
      echo("</td><td>");
      echo('<b>'."Headline".'</b>');
      echo("</td><td>");
      echo('<b>'."Action".'</b>');

      echo("</td></tr>\n");

      foreach ( $rows as $row)  {
          $rowcounter++;
          echo "<tr><td>";
          echo('<a href="view.php?profile_id='.$row['profile_id'].'">'.htmlentities($row['first_name']).'</a>');
//          echo(htmlentities($row['first_name']));
          echo("</td><td>");
          echo(htmlentities($row['headline']));
          echo("</td><td>");
          echo('<a href="edit.php?profile_id='.$row['profile_id'].'">Edit</a> / ');
          echo('<a href="delete.php?profile_id='.$row['profile_id'].'">Delete</a>');
          echo("</td></tr>\n");
      }
      echo('</table>'."\n");
    } // End if($rowcounter != 0)

    echo ('<p> <a href="add.php">Add New Entry</a> </p>');
  }
?>

</div>
</body>
</html>
