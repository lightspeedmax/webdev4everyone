<?php
require_once "pdo.php";
require_once "util.php";
//require_once "head.php";
session_start();

die_if_not_logged_in();

  // If the user requested logout go back to index.php
  if ( isset($_POST['cancel']) ) {
      header('Location: index.php');
      return;
  }

//  if { isset($_POST['Add']) )
$isDataSet = FALSE;
$isDataNumeric = FALSE;
$isMakeValid = FALSE;
$isRecordInserted = FALSE;
$isPositionsValid = FALSE;
$isEducationValid = FALSE;

if ( isset($_POST['add']) )
{
  if ( isset($_POST['first_name']) &&
       isset($_POST['last_name']) &&
       isset($_POST['headline']) &&
       isset($_POST['summary']) &&
       isset($_POST['email']))
  {
    $isDataSet = TRUE;
  }

  if ((strlen($_POST['first_name']) > 1) &&
      (strlen($_POST['last_name']) > 1) &&
      (strlen($_POST['email']) > 1) &&
      (strlen($_POST['headline']) > 1) &&
      (strlen($_POST['summary']) > 1)
      )
  //if (strlen($_POST['make']) < 1)
  {
    $substr = "@";
    if(strpos($_POST['email'], $substr) !== false)
    {
      $isMakeValid = TRUE;
    }
    else
    {
      $_SESSION["error"] = "Bad email Address";
      header( 'Location: add.php' ) ;// header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
      return;
    }

    $PositionValidationCheck = validatePos();
    if ($PositionValidationCheck === TRUE)
    {
      $isPositionsValid = TRUE;
      $isDataNumeric = TRUE;
    }
    else
    {
      //echo('<p style="color: red;">'.htmlentities("Mileage and year must be numeric")."</p>\n");
      $_SESSION["error"] = $PositionValidationCheck;
      header( 'Location: add.php' ) ;//header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
      return;
    }

    $EducationValidationCheck = validateEdu();
    if ($EducationValidationCheck === TRUE)
    {
      $isEducationValid = TRUE;
      $isDataNumeric = TRUE;
    }
    else
    {
      //echo('<p style="color: red;">'.htmlentities("Mileage and year must be numeric")."</p>\n");
      $_SESSION["error"] = $EducationValidationCheck;
      header( 'Location: add.php' ) ;//header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
      return;
    }

  }
  else
  {
    $_SESSION["error"] = "All values are required";//"Make is required";
    header( 'Location: add.php' ) ;
    return;
  }

  if(($isDataSet === TRUE)        &&
     ($isDataNumeric === TRUE)    &&
     ($isMakeValid === TRUE)      &&
     ($isPositionsValid === TRUE) &&
     ($isEducationValid === TRUE)
     )
  {
    PesistResume($pdo);
    $profile_id = $pdo->lastInsertId();
    PersistPositions($pdo,$profile_id);

    PersistEducation($pdo,$profile_id);
    $isRecordInserted = TRUE;
    $_SESSION["success"] = "added for Profile ID".$profile_id;//"Record inserted";
    header( 'Location: index.php' ) ;
    return;
  }
}
else
{
    echo('<p style="color: blue;">'.htmlentities("...")."</p>\n");
}



//----------------View------------------------
?>
<html>
<head>
  <?php //require_once "bootstrap.php"; ?>
  <title>Craig Mullins Resume Profile</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
</head>
<body style="font-family: sans-serif;">
<div class="container">
<?php
echo ('<h1>Adding Profile for '.$_SESSION["account"].'</h1>');
/* Valid if a user has actually logged in */
    if ( isset($_SESSION["error"]) ) {
        echo('<p style="color:red">'.$_SESSION["error"]."</p>\n");
        unset($_SESSION["error"]);
    }
?>
<form method="post">
<p>First Name:
<input type="text" name="first_name" size="60"/></p>
<p>Last Name:
<input type="text" name="last_name" size="60"/></p>
<p>Email:
<input type="text" name="email" size="40"/></p>
<p>Headline:
<input type="text" name="headline" size="40"/></p>
<p>Summary:
<br>
<textarea name="summary" rows="4" cols="60"> </textarea>
<br>
<p>
Education: <input type="submit" id="addEdu" value="+">
<div id="edu_fields">
</div>
</p>
<p>Position: <input type="submit" id="addPos" value="+">
<div id="position_fields">
</div>
</p>
<input type="submit" name="add" value="Add">
<input type="submit" name="cancel" value="Cancel">
</form>

<script>
countPos = 0;
countEduPos = 0;

// http://stackoverflow.com/questions/17650776/add-remove-html-inside-div-using-javascript
$(document).ready(function(){
    window.console && console.log('Document ready called');
    $('#addPos').click(function(event){
        // http://api.jquery.com/event.preventdefault/
        event.preventDefault();
        if ( countPos >= 9 ) {
            alert("Maximum of nine position entries exceeded");
            return;
        }
        countPos++;
        window.console && console.log("Adding position "+countPos);
        $('#position_fields').append(
            '<div id="position'+countPos+'"> \
            <p>Year: <input type="text" name="year'+countPos+'" value="" /> \
            <input type="button" value="-" \
                onclick="$(\'#position'+countPos+'\').remove();return false;"></p> \
            <textarea name="desc'+countPos+'" rows="8" cols="80"></textarea>\
            </div>');
    });

    $('#addEdu').click(function(event){
        // http://api.jquery.com/event.preventdefault/
        event.preventDefault();
        if ( countEduPos >= 9 ) {
            alert("Maximum of nine position entries exceeded");
            return;
        }
        countEduPos++;
        window.console && console.log("Adding position "+countEduPos);
        $('#edu_fields').append(
            '<div id="position'+countEduPos+'"> \
            <p>Year: <input type="text" name="edu_year'+countEduPos+'" value="" /> \
            <input type="button" value="-" \
                onclick="$(\'#position'+countEduPos+'\').remove();return false;"></p> \
            <p>School: <input type="text" size="80" name="edu_school'+countEduPos+'" class="school" value="" /> \
            </div>');

        $('.school').autocomplete({
                source: "school.php"
            });
    });

    $('.school').autocomplete({
            source: "school.php"
        });
});
</script>

</div>
</body>
</html>
