<?php
require_once "pdo.php";
    session_start();

    if ( ! isset($_SESSION["account"]) ) {
      die('Not logged in');
    }

//----------------View------------------------
?>
<html>
<head>
  <?php// require_once "bootstrap.php"; ?>
  <title>>Tracking Autos for Craig Mullins</title>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<script  src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

</head>
<body style="font-family: sans-serif;">
<h1>Profile information</h1>
<p>
<?php
    if ( isset($_SESSION["success"]) ) {
        echo('<p style="color: green;">'.htmlentities($_SESSION['success'])."</p>\n");
        unset($_SESSION["success"]);
    }

$stmt = $pdo->prepare("SELECT * FROM profile where profile_id = :xyz");
$stmt->execute(array(":xyz" => $_GET['profile_id']));
//var_dump($stmt);
$rowPro = $stmt->fetchAll(PDO::FETCH_ASSOC);
if ( $rowPro === false ) {
  echo "No Profile Data";
}
//echo "<pre>"; print_r($rowPro) ;  echo "</pre>";

//echo "<pre>"; print_r($rowPro[0]['first_name']) ;  echo "</pre>";

$fn = htmlentities($rowPro[0]['first_name']);
$ln = htmlentities($rowPro[0]['last_name']);
$email = htmlentities($rowPro[0]['email']);
$head = htmlentities($rowPro[0]['headline']);
$sum = htmlentities($rowPro[0]['summary']);

$profile_id = $_GET['profile_id'];

//$rowPositions = GetPositions($pdo,$profile_id);
$stmtPosition = $pdo->prepare("SELECT year,description FROM position where profile_id = :xyz");
$stmtPosition ->execute(array(":xyz" => $profile_id));
$rowPositions = $stmtPosition->fetchAll(PDO::FETCH_ASSOC);

$stmtEducation = $pdo->prepare("SELECT education.year,institution.name
                                FROM education, institution
                                where profile_id = :xyz
                                AND education.institution_id = institution.institution_id");
$stmtEducation ->execute(array(":xyz" => $profile_id));
$rowEducation = $stmtEducation->fetchAll(PDO::FETCH_ASSOC);

echo('<p>First Name: '.$fn.'</p>');
echo('<p>Last Name: '.$ln.'</p>');
echo('<p>Email : '.$email.'</p>');
echo('<p>Headline:<br/>'.$head.'</p>');
echo('<p>Summary:<br/>'.$sum.'</p>');

echo('<p>Education:</p><ul>');
if($rowEducation !== FALSE ) {
  foreach ( $rowEducation as $rowPos ) {
    $year = htmlentities($rowPos['year']);
    $description = htmlentities($rowPos['name']);
      echo "<li>";
      echo($year);
      echo(": ");
      echo($description);
      echo("</li>\n");
  }
}
echo("</ul>\n");

echo('<p>Position:</p><ul>');
//echo "<p>Querying for data... </p>";

if($rowPositions !== FALSE ) {
  foreach ( $rowPositions as $rowPos ) {
    $year = htmlentities($rowPos['year']);
    $description = htmlentities($rowPos['description']);
      echo "<li>";
      echo($year);
      echo(": ");
      echo($description);
      echo("</li>\n");
  }
}
echo("</ul>\n");
?>
</p>
      <p> <a href="index.php">Done</a></p>

</body>
</html>
