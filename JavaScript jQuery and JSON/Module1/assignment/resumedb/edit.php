<?php
require_once "pdo.php";
session_start();

if ( ! isset($_SESSION["account"]) ) {
  die('ACCESS DENIED');
}

// Guardian: Make sure that user_id is present
if ( ! isset($_GET['profile_id']) ) {
  $_SESSION['error'] = "Missing profile_id";
  header('Location: index.php');
  return;
}

// If the user requested logout go back to index.php
if ( isset($_POST['cancel']) ) {
    header('Location: index.php');
    return;
}

//  if { isset($_POST['Add']) )
$isDataSet = FALSE;
$isDataNumeric = FALSE;
$isMakeValid = FALSE;
$isRecordInserted = FALSE;

if ( isset($_POST['first_name']) &&
     isset($_POST['last_name']) &&
     isset($_POST['headline']) &&
     isset($_POST['summary']) &&
     isset($_POST['email']))
{
//  if ( isset($_POST['make']) &&
//       isset($_POST['model']) &&
//       isset($_POST['year']) &&
//       isset($_POST['mileage']))
  {
    $isDataSet = TRUE;
  }

  if ((strlen($_POST['first_name']) > 1) &&
      (strlen($_POST['last_name']) > 1) &&
      (strlen($_POST['email']) > 1) &&
      (strlen($_POST['headline']) > 1) &&
      (strlen($_POST['summary']) > 1)
      )
  //if (strlen($_POST['make']) < 1)
  {
    $substr = "@";
    if(strpos($_POST['email'], $substr) !== false)
    {
      $isMakeValid = TRUE;
    }
    else
    {
      $_SESSION["error"] = "Bad email Address";
      header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
      return;
    }
    //if ( is_numeric($_POST['year']) &&
    //     is_numeric($_POST['mileage']))
    {
      $isDataNumeric = TRUE;
    }
    //else
    {
      //echo('<p style="color: red;">'.htmlentities("Mileage and year must be numeric")."</p>\n");
    //  $_SESSION["error"] = "Mileage and year must be numeric";
    //  header( "Location: edit.php?autos_id=".$_POST['autos_id'] ) ;
    //  return;
    //  break;
    }
  }
  else
  {
    //echo('<p style="color: red;">'.htmlentities("Make is required")."</p>\n");
    $_SESSION["error"] = "All values are required";
    header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
    return;
  }

  if(($isDataSet === TRUE) &&
     ($isDataNumeric === TRUE) &&
     ($isMakeValid === TRUE))
  {
    $sql = "UPDATE profile SET first_name =:first_name,
                            last_name =:last_name,
                            email =:email,
                            headline =:headline,
                            summary =:summary
              WHERE profile_id = :profile_id";


    //echo("<pre>\n".$sql."\n</pre>\n");
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(
        ':first_name' => htmlentities($_POST['first_name']),
        ':last_name' => htmlentities($_POST['last_name']),
        ':email' => htmlentities($_POST['email']),
        ':headline' => htmlentities($_POST['headline']),
        ':summary' => htmlentities($_POST['summary']),
        ':profile_id' => $_POST['profile_id']));
    $isRecordInserted = TRUE;

    $_SESSION["success"] = "Record updated";
    header( 'Location: index.php' ) ;
    return;

  }
}
else
{
    echo('<p style="color: blue;">'.htmlentities("...")."</p>\n");
}

$stmt = $pdo->prepare("SELECT * FROM profile where profile_id = :xyz");
$stmt->execute(array(":xyz" => $_GET['profile_id']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if ( $row === false ) {
    $_SESSION['error'] = 'Bad value';
    header( 'Location: index.php' ) ;
    return;
}

// Flash pattern
//if ( isset($_SESSION['error']) ) {
//    echo '<p style="color:red">'.$_SESSION['error']."</p>\n";
//    unset($_SESSION['error']);
//}

$fn = htmlentities($row['first_name']);
$ln = htmlentities($row['last_name']);
$email = htmlentities($row['email']);
$head = htmlentities($row['headline']);
$sum = htmlentities($row['summary']);
$profile_id = $row['profile_id'];

//----------------View------------------------
?>
<html>
<head>
  <?php //require_once "bootstrap.php"; ?>
  <title>Craig Mullins Resume Profile</title>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

</head>
<body style="font-family: sans-serif;">
<?php
echo ('<h1>Editing Profile for '.$fn.'</h1>');
/* Valid if a user has actually logged in */
    if ( isset($_SESSION["error"]) ) {
        echo('<p style="color:red">'.$_SESSION["error"]."</p>\n");
        unset($_SESSION["error"]);
    }
?>

<form method="post">
<p>First Name:
<input type="text" name="first_name" value="<?= $fn ?>" size="60"/></p>
<p>Last Name:
<input type="text" name="last_name" value="<?= $ln ?>" size="60"/></p>
<p>Email:
<input type="text" name="email" value="<?= $email ?>" size="40"/></p>
<p>Headline:
<input type="text" name="headline" value="<?= $head ?>" size="40"/></p>
<p>Summary:
<br>
<textarea name="summary" rows="4" cols="60"><?=$sum ?></textarea>
<br>
<input type="hidden" name="profile_id" value="<?=$profile_id?>">
<input type="submit" name="edit" value="Save"/>
<input type="submit" name="cancel" value="Cancel">
</form>

</body>
</html>
