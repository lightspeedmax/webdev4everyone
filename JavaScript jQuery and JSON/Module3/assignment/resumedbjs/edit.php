<?php
require_once "pdo.php";
session_start();

if ( ! isset($_SESSION["account"]) ) {
  die('ACCESS DENIED');
}

// Guardian: Make sure that user_id is present
if ( ! isset($_GET['profile_id']) ) {
  $_SESSION['error'] = "Missing profile_id";
  header('Location: index.php');
  return;
}

// If the user requested logout go back to index.php
if ( isset($_POST['cancel']) ) {
    header('Location: index.php');
    return;
}

//  if { isset($_POST['Add']) )
$isDataSet = FALSE;
$isDataNumeric = FALSE;
$isMakeValid = FALSE;
$isRecordInserted = FALSE;
$isPositionsValid = FALSE;

if ( isset($_POST['first_name']) &&
     isset($_POST['last_name']) &&
     isset($_POST['headline']) &&
     isset($_POST['summary']) &&
     isset($_POST['email']))
{
//  if ( isset($_POST['make']) &&
//       isset($_POST['model']) &&
//       isset($_POST['year']) &&
//       isset($_POST['mileage']))
  {
    $isDataSet = TRUE;
  }

  if ((strlen($_POST['first_name']) > 1) &&
      (strlen($_POST['last_name']) > 1) &&
      (strlen($_POST['email']) > 1) &&
      (strlen($_POST['headline']) > 1) &&
      (strlen($_POST['summary']) > 1)
      )
  //if (strlen($_POST['make']) < 1)
  {
    $substr = "@";
    if(strpos($_POST['email'], $substr) !== false)
    {
      $isMakeValid = TRUE;
    }
    else
    {
      $_SESSION["error"] = "Bad email Address";
      header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
      return;
    }

    $PositionValidationCheck = validatePos();
    if ($PositionValidationCheck === TRUE)
    {
      $isPositionsValid = TRUE;
      $isDataNumeric = TRUE;
    }
    else
    {
      //echo('<p style="color: red;">'.htmlentities("Mileage and year must be numeric")."</p>\n");
      $_SESSION["error"] = $PositionValidationCheck;
      header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
      return;
    }
  }
  else
  {
    //echo('<p style="color: red;">'.htmlentities("Make is required")."</p>\n");
    $_SESSION["error"] = "All values are required";
    header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
    return;
  }

//  echo "<pre>"; print_r($_POST) ;  echo "</pre>";

  if(($isDataSet === TRUE) &&
     ($isDataNumeric === TRUE) &&
     ($isMakeValid === TRUE) &&
     ($isPositionsValid === TRUE))
  {
    $sql = "UPDATE profile SET first_name =:first_name, last_name =:last_name,
                            email =:email,
                            headline =:headline,
                            summary =:summary
              WHERE profile_id = :profile_id";


    //echo("<pre>\n".$sql."\n</pre>\n");
    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(
        ':first_name' => htmlentities($_POST['first_name']),
        ':last_name' => htmlentities($_POST['last_name']),
        ':email' => htmlentities($_POST['email']),
        ':headline' => htmlentities($_POST['headline']),
        ':summary' => htmlentities($_POST['summary']),
        ':profile_id' => $_POST['profile_id']));

    $profile_id = $_POST['profile_id'];

    // Clear out the old position entries
    $stmtDelete = $pdo->prepare('DELETE FROM position WHERE profile_id=:pid');
    $stmtDelete->execute(array( ':pid' => $_REQUEST['profile_id']));


    // Now add the Positions to the postions table
    $rank = 1;

    for($i=1; $i<=10; $i++)
    {
      if ( ! isset($_POST['year'.$i]) ) continue;
      if ( ! isset($_POST['desc'.$i]) ) continue;

      $year = $_POST['year'.$i];
      $desc = $_POST['desc'.$i];

      $sqlPosition = "INSERT INTO position (profile_id, rank, year, description)
                              VALUES ( :pid, :rank, :year, :desc)";

      $stmtPosition = $pdo->prepare($sqlPosition);

      $stmtPosition->execute(array(
        ':pid' => $profile_id,
        ':rank' => $rank,
        ':year' => $year,
        ':desc' => $desc)
      );
      $rank++;
    }

    $isRecordInserted = TRUE;

    $_SESSION["success"] = "Record updated Profile ID".$profile_id." Rank Count:".$rank;

    header( 'Location: index.php' ) ;
    return;

  //  echo "<pre>"; print_r($_POST) ;  echo "</pre>";

  }
  else
  {
  //    echo "<pre>"; print_r($_POST) ;  echo "</pre>";

  }
}
else
{
    echo('<p style="color: blue;">'.htmlentities("...")."</p>\n");

    // For testing:
//    var_dump($_POST);

}

$stmt = $pdo->prepare("SELECT * FROM profile where profile_id = :xyz");
$stmt->execute(array(":xyz" => $_GET['profile_id']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);
if ( $row === false ) {
    $_SESSION['error'] = 'Bad value';
    header( 'Location: index.php' ) ;
    return;
}

$stmtPosition = $pdo->prepare("SELECT year,description FROM position where profile_id = :xyz");
$stmtPosition ->execute(array(":xyz" => $_GET['profile_id']));

$rowPositions = $stmtPosition->fetchAll(PDO::FETCH_ASSOC);

$positionRowcounter = 0;
if ( $rowPositions !== false )
{
  foreach ( $rowPositions as $rowsToCount)
  {
//      echo("Counting: ".$rowsToCount);
      $positionRowcounter++;
  }
}

function validatePos() {

//  return "Never Valid";

  for($i=1; $i<=9; $i++) {
    if ( ! isset($_POST['year'.$i]) ) continue;
    if ( ! isset($_POST['desc'.$i]) ) continue;

    $year = $_POST['year'.$i];
    $desc = $_POST['desc'.$i];

    if ( strlen($year) == 0 || strlen($desc) == 0 ) {
      return "All fields are required";
    }

    if ( ! is_numeric($year) ) {
      return "Position year must be numeric";
    }
  }
  return true;
}


$fn = htmlentities($row['first_name']);
$ln = htmlentities($row['last_name']);
$email = htmlentities($row['email']);
$head = htmlentities($row['headline']);
$sum = htmlentities($row['summary']);
$profile_id = $row['profile_id'];

//----------------View------------------------
?>
<html>
<head>
  <?php //require_once "bootstrap.php"; ?>
  <title>Craig Mullins Resume Profile</title>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

</head>
<body style="font-family: sans-serif;">

<?php
echo ('<h1>Editing Profile for Me '.$fn.'</h1>');
/* Valid if a user has actually logged in */
    if ( isset($_SESSION["error"]) ) {
        echo('<p style="color:red">'.$_SESSION["error"]."</p>\n");
        unset($_SESSION["error"]);
    }
?>

<form method="post">
<p>First Name:
<input type="text" name="first_name" value="<?= $fn ?>" size="60"/></p>
<p>Last Name:
<input type="text" name="last_name" value="<?= $ln ?>" size="60"/></p>
<p>Email:
<input type="text" name="email" value="<?= $email ?>" size="40"/></p>
<p>Headline:
<input type="text" name="headline" value="<?= $head ?>" size="40"/></p>
<p>Summary:
<br>
<textarea name="summary" rows="4" cols="60"><?=$sum ?></textarea>
<br>
<input type="hidden" name="profile_id" value="<?=$profile_id?>">

<p>Position: <input type="submit" id="addPos" value="+">
<div id="position_fields">
<?php

//echo("Counter: ".$positionRowcounter);

  if($positionRowcounter != 0)
  {
    $rowcounter=1;
    foreach ( $rowPositions as $row)
    {
      $year = htmlentities($row['year']);
      $description = htmlentities($row['description']);

      echo('<div id="position'.$rowcounter.'">'."\n");
      echo('<p>Year: <input type="text" name="year'.$rowcounter.'" value="'.$year.'" />'."\n");
      echo('<input type="button" value="-" onclick="$(\'#position'.$rowcounter.'\').remove();return false;">'."\n");
      echo('</p>'."\n");

      echo('<textarea name="desc'.$rowcounter.'" rows="8" cols="80">'."\n");
      echo($description );
      echo('</textarea>'."\n");
      echo('</div>'."\n");
      $rowcounter++;
    }
  }
  /*
  <div id="position1">
  <p>Year: <input type="text" name="year1" value="234" />
  <input type="button" value="-" onclick="$('#position1').remove();return false;">
  </p>
  <textarea name="desc1" rows="8" cols="80">
  ddfdf

  </textarea>
  </div>
  */
?>
</div>
</p>

<input type="submit" name="edit" value="Save"/>
<input type="submit" name="cancel" value="Cancel">
</form>

<script>
countPos = 0;

// http://stackoverflow.com/questions/17650776/add-remove-html-inside-div-using-javascript
$(document).ready(function(){
    window.console && console.log('Document ready called');

    // Once document has loaded, we need to somehow check what the last position
    // rank number is
    var i;
    for (i = 0; i <= 9; i++) {
      if(document.getElementById('position'+i) !== null)
      {
        window.console && console.log('Found ID tag position'+i);
        countPos = i+1;
      }
    }

    window.console && console.log('Current position Count is '+countPos);



    // Now register a callback event
    $('#addPos').click(function(event){
        // http://api.jquery.com/event.preventdefault/
        event.preventDefault();
        if ( countPos >= 10 ) {
            alert("Maximum of nine position entries exceeded");
            return;
        }
        countPos++;
        window.console && console.log("Adding position "+countPos);
        $('#position_fields').append(
            '<div id="position'+countPos+'"> \
            <p>Year: <input type="text" name="year'+countPos+'" value="" /> \
            <input type="button" value="-" \
                onclick="$(\'#position'+countPos+'\').remove();return false;"></p> \
            <textarea name="desc'+countPos+'" rows="8" cols="80"></textarea>\
            </div>');
    });
});
</script>

</body>
</html>
