<?php
require_once "pdo.php";
  session_start();

  if ( ! isset($_SESSION["account"]) ) {
    die('ACCESS DENIED');
  }

  // If the user requested logout go back to index.php
  if ( isset($_POST['cancel']) ) {
      header('Location: index.php');
      return;
  }

//  if { isset($_POST['Add']) )
$isDataSet = FALSE;
$isDataNumeric = FALSE;
$isMakeValid = FALSE;
$isRecordInserted = FALSE;
$isPositionsValid = FALSE;

if ( isset($_POST['add']) )
{
  if ( isset($_POST['first_name']) &&
       isset($_POST['last_name']) &&
       isset($_POST['headline']) &&
       isset($_POST['summary']) &&
       isset($_POST['email']))
  {
    $isDataSet = TRUE;
  }

  if ((strlen($_POST['first_name']) > 1) &&
      (strlen($_POST['last_name']) > 1) &&
      (strlen($_POST['email']) > 1) &&
      (strlen($_POST['headline']) > 1) &&
      (strlen($_POST['summary']) > 1)
      )
  //if (strlen($_POST['make']) < 1)
  {
    $substr = "@";
    if(strpos($_POST['email'], $substr) !== false)
    {
      $isMakeValid = TRUE;
    }
    else
    {
      $_SESSION["error"] = "Bad email Address";
      header( 'Location: add.php' ) ;// header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
      return;
    }

    $PositionValidationCheck = validatePos();
    if ($PositionValidationCheck === TRUE)
    {
      $isPositionsValid = TRUE;
      $isDataNumeric = TRUE;
    }
    else
    {
      //echo('<p style="color: red;">'.htmlentities("Mileage and year must be numeric")."</p>\n");
      $_SESSION["error"] = $PositionValidationCheck;
      header( 'Location: add.php' ) ;//header( "Location: edit.php?profile_id=".$_POST['profile_id'] ) ;
      return;
    }
  }
  else
  {
    $_SESSION["error"] = "All values are required";//"Make is required";
    header( 'Location: add.php' ) ;
    return;
  }

  if(($isDataSet === TRUE) &&
     ($isDataNumeric === TRUE) &&
     ($isMakeValid === TRUE))
  {
    $sql = "INSERT INTO profile ( user_id, first_name, last_name, email, headline, summary)
              VALUES (:user_id, :first_name, :last_name, :email, :headline, :summary)";

    $stmt = $pdo->prepare($sql);
    $stmt->execute(array(
        ':first_name' => htmlentities($_POST['first_name']),
        ':last_name'  => htmlentities($_POST['last_name']),
        ':email'      => htmlentities($_POST['email']),
        ':headline'   => htmlentities($_POST['headline']),
        ':summary'    => htmlentities($_POST['summary']),
        ':user_id'    => $_SESSION['account']));

    $profile_id = $pdo->lastInsertId();


    $rank = 1;

// Now add the Positions to the postions table

    for($i=1; $i<=9; $i++)
    {
      if ( ! isset($_POST['year'.$i]) ) continue;
      if ( ! isset($_POST['desc'.$i]) ) continue;

      $year = $_POST['year'.$i];
      $desc = $_POST['desc'.$i];

      $sqlPosition = "INSERT INTO position (profile_id, rank, year, description)
                              VALUES ( :pid, :rank, :year, :desc)";

      $stmtPosition = $pdo->prepare($sqlPosition);

      $stmtPosition->execute(array(
        ':pid' => $profile_id,
        ':rank' => $rank,
        ':year' => $year,
        ':desc' => $desc)
      );
      $rank++;
    }

    $isRecordInserted = TRUE;

    $_SESSION["success"] = "added for Profile ID".$profile_id;//"Record inserted";
    header( 'Location: index.php' ) ;
    return;
  }
}
else
{
    echo('<p style="color: blue;">'.htmlentities("...")."</p>\n");
}

function validatePos()
{
//  return "Never Valid";
  for($i=1; $i<=9; $i++) {
    if ( ! isset($_POST['year'.$i]) ) continue;
    if ( ! isset($_POST['desc'.$i]) ) continue;

    $year = $_POST['year'.$i];
    $desc = $_POST['desc'.$i];

    if ( strlen($year) == 0 || strlen($desc) == 0 ) {
      return "All fields are required";
    }

    if ( ! is_numeric($year) ) {
      return "Position year must be numeric";
    }
  }
  return true;
}

//----------------View------------------------
?>
<html>
<head>
  <?php //require_once "bootstrap.php"; ?>
  <title>Craig Mullins Resume Profile</title>
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>

</head>
<body style="font-family: sans-serif;">
<div class="container">
<?php
echo ('<h1>Adding Profile for '.$_SESSION["account"].'</h1>');
/* Valid if a user has actually logged in */
    if ( isset($_SESSION["error"]) ) {
        echo('<p style="color:red">'.$_SESSION["error"]."</p>\n");
        unset($_SESSION["error"]);
    }
?>
<form method="post">
<p>First Name:
<input type="text" name="first_name" size="60"/></p>
<p>Last Name:
<input type="text" name="last_name" size="60"/></p>
<p>Email:
<input type="text" name="email" size="40"/></p>
<p>Headline:
<input type="text" name="headline" size="40"/></p>
<p>Summary:
<br>
<textarea name="summary" rows="4" cols="60"> </textarea>
<br>
<p>Position: <input type="submit" id="addPos" value="+">
<div id="position_fields">
</div>
</p>
<input type="submit" name="add" value="Add">
<input type="submit" name="cancel" value="Cancel">
</form>

<script>
countPos = 0;

// http://stackoverflow.com/questions/17650776/add-remove-html-inside-div-using-javascript
$(document).ready(function(){
    window.console && console.log('Document ready called');
    $('#addPos').click(function(event){
        // http://api.jquery.com/event.preventdefault/
        event.preventDefault();
        if ( countPos >= 9 ) {
            alert("Maximum of nine position entries exceeded");
            return;
        }
        countPos++;
        window.console && console.log("Adding position "+countPos);
        $('#position_fields').append(
            '<div id="position'+countPos+'"> \
            <p>Year: <input type="text" name="year'+countPos+'" value="" /> \
            <input type="button" value="-" \
                onclick="$(\'#position'+countPos+'\').remove();return false;"></p> \
            <textarea name="desc'+countPos+'" rows="8" cols="80"></textarea>\
            </div>');
    });
});
</script>

</div>
</body>
</html>
