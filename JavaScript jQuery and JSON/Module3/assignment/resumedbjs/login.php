<?php
require_once "pdo.php";
session_start();
/*--------------------Model ----------------------*/
/* Variables */
$salt = 'XyZzy12*_';
$stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';  // Pw is php123
$failure = false;  // If we have no POST data

/* Handle the Cancel Button */
if ( isset($_POST['cancel'] ) ) {
    header("Location: index.php");
    return;
}

/* Checking for Valid email and Password */
if ( isset($_POST['email']) && isset($_POST['pass'])  ) {

    unset($_SESSION["account"]);  // Logout current user

    if ( strlen($_POST['email']) < 1 || strlen($_POST['pass']) < 1 ) {
        $_SESSION["error"] = "Email and password required";
        header( 'Location: login.php' ) ;
        return;
    } else {

      $substr = "@";
      if(strpos($_POST['email'], $substr) !== false) {
        // Now query the Access details and confirm validity
        $check = hash('md5', $salt.$_POST['pass']);
        $sql = "SELECT user_id,name,email,password FROM users
            WHERE email = :em AND password = :pw";
        $stmt = $pdo->prepare($sql);
        $stmt->execute(array(
            ':em' => $_POST['email'],
            ':pw' => $check));
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $name = $row['name'];
        $user_id = $row['user_id'];

        if ( $row == TRUE ) {

            $logStatus = error_log("Login success ".$_POST['email'],0);//, 3, "c
            $_SESSION["account"] = $user_id;;//$_POST["email"];
            $_SESSION["success"] = "Logged in.";
            //$_SESSION["user_id"] = $user_id;
            header( 'Location: index.php' ) ;
            return;
        } else {
            error_log("Login fail ".$_POST['email']." $check", 0);
            $_SESSION["error"] = "Incorrect password";
            header( 'Location: login.php' ) ;
            return;
        }
      } else {
        $_SESSION["error"] = "Email must have an at-sign (@)";
        header( 'Location: login.php' ) ;
        return;
      }
    }
}

/*-------------------- View ----------------------*/
?>
<!DOCTYPE html>
<html>
<head>
<?php require_once "bootstrap.php"; ?>
<title>Craig Mullins - Automobile Database</title>
</head>
<body>
<div class="container">
<h1>Please Log In</h1>
<?php
/* Valid if a user has actually logged in */
    if ( isset($_SESSION["error"]) ) {
        echo('<p style="color:red">'.$_SESSION["error"]."</p>\n");
        unset($_SESSION["error"]);
    }
?>

<script type="text/javascript">
function product(a,b) {
   value = a + b;
   return value;
}
//console.log("Prod = "+product(4,5));

function doValidate()
{
  var pw_valid = false;
  var nam_valid = false;
  var result = false;
  console.log('Validating...');

//  alert('Hi'); return false;

  try {
    pw = document.getElementById('id_1723').value;
    user = document.getElementById('id_nam').value;
    //console.log("Validating pw="+pw);
    if (pw == null || pw == "") {
      alert("Password must be filled out");
    }
    else
    {
      console.log("Password has been validated");
      //return true;
      pw_valid = true;
    }

    if (user == null || user == "") {
      alert("Email must be filled out");
    }
    else
    {
      nam_valid = true;
    }
  } catch(e) {
    result = false;
  }
  if(pw_valid && nam_valid)
  {
    result = true;
  }
  return result;
}
</script>


<form method="POST" action="login.php">
<label for="id_nam">User Name</label>
<input type="text" name="email" id="id_nam"><br/>
<label for="id_1723">Password</label>
<input type="password" name="pass" id="id_1723"><br/>
<input type="submit" value="Log In" onclick="doValidate();">
<input type="submit" name="cancel" value="Cancel">

</form></div>
</body>
